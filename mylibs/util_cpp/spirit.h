#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>


// http://stackoverflow.com/questions/3066701/boost-spirit-semantic-action-parameters


namespace and_spirit {
	using boost::spirit::qi::lexeme;
	using boost::spirit::standard::char_;
	using boost::spirit::qi::int_;
	using namespace boost::spirit::qi::labels;
	using boost::phoenix::at_c;
	using boost::phoenix::push_back;
	using boost::spirit::qi::lit;
	using boost::spirit::qi::string;
	using boost::spirit::qi::eol;
	using boost::spirit::qi::space;
	
	template <typename NodeType>
	struct ast {
		typedef NodeType node_type;
		std::string name;
		std::vector<ast> children;
		NodeType type;

		auto begin() const -> decltype(children.begin()) { return children.begin(); }
		auto end() const -> decltype(children.end()) { return children.end(); }
	};
	
	template <typename Ast>
	using context = boost::spirit::context<
		boost::fusion::cons<Ast&, boost::fusion::nil>,
		boost::fusion::vector0<> >;

	
	template <typename NodeType, NodeType type>
	void emit(const std::vector<char>& a, const context<ast<NodeType>>& ctx, bool& mFlag) {
		std::string s;
		s.append(&a[0], a.size());
		(boost::fusion::at_c<0>(ctx.attributes)).name += s;
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}
	template <typename NodeType, NodeType type>
	void emit_str(const std::string& s, const context<ast<NodeType>>& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).name += s;
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}

	template <typename NodeType, NodeType type>
	void emitnl(const std::vector<char>& a, const context<ast<NodeType>>& ctx, bool& mFlag) {
		std::string s;
		s.append(&a[0], a.size());
		s.append("\n");
		(boost::fusion::at_c<0>(ctx.attributes)).name += s;
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}


	template <typename Ast>
	void copy_data(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).name = a.name;
		(boost::fusion::at_c<0>(ctx.attributes)).type = a.type;
	}

	template <typename Ast, typename NodeType, NodeType type>
	void copy_name(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).name = a.name;
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}

	
	template <typename Ast>
	void copy_children(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).children = a.children;
	}
	
	template <typename Ast>
	void copy_all(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).name = a.name;
		(boost::fusion::at_c<0>(ctx.attributes)).type = a.type;
		(boost::fusion::at_c<0>(ctx.attributes)).children = a.children;
	}
	
	template <typename Ast, typename NodeType, NodeType type>
	void copy_set_type(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).name = a.name;
		(boost::fusion::at_c<0>(ctx.attributes)).children = a.children;
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}
	

	template <typename Ast>
	void push(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		if (a.type != Ast::node_type::DEFAULT)
			(boost::fusion::at_c<0>(ctx.attributes)).children.push_back(a);
	}
	
	template <typename Ast, typename NodeType, NodeType type>
	void push_set_type(const Ast& a, const context<Ast>& ctx, bool& mFlag) {
		if (a.type != Ast::node_type::DEFAULT)
			(boost::fusion::at_c<0>(ctx.attributes)).children.push_back(a);
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}
	
	void print(const boost::spirit::unused_type& a, const boost::spirit::unused_type& ctx, bool& mFlag) {
		std::cout<<"*";
	}
	void print(const std::vector<char>& a, const boost::spirit::unused_type& ctx, bool& mFlag) {
		std::string s;
		s.append(&a[0], a.size());
		std::cout<<s;
	}




	//replace indentation with INDENT/DEDENT tokens..
	// todo: exclude_region : use regex..
	bool indent(std::istream & iss, std::string & out, const char * INDENT, const char * DEDENT, const std::pair<const char *, const char *>& exclude_region = std::pair<const char*, const char*>(nullptr, nullptr)) {
		int level = 0;
		int count = 0;
		bool skip_region = false;
		for (std::string line; std::getline(iss, line);) {
			if (exclude_region.first != nullptr && line.find(exclude_region.first) != std::string::npos){
				skip_region = true;
			}
			if (skip_region) continue;
			
			for (auto c : line)			    { if (c == '\t') count++; else break; }
			if (count == 0 && level == 0) 	{ out += line + "\n"; continue; }
			if (count >0)					line = line.replace(0, count, ""); 
			if (count > level) {
				if (count - level > 1) return false;
				level++;
				out += INDENT + line + "\n";
			} else if (count == level) {
				out += line + "\n";
			} else if (count < level) {
				line += "\n";
				for (int j=0;j<level-count;j++) line = DEDENT + line;
				out += line;
				level -= (level-count);
			}
			count = 0;

			if (exclude_region.second != nullptr && line.find(exclude_region.second) == std::string::npos){
				skip_region = false;
			}
		}
		return true;
	}

	
	
	
	template <typename Ast>
	void print_ast(const Ast& root, const std::string& name = "") {
		std::cout<<"-- ast "<< name << " --\n";
		and_util::walk(root, 
			static_cast<void (*)(const Ast&, int&)>(
			[](const Ast& n, int& level){
				for (int i=0; i<level; ++i) {
					std::cout<<"  ";
				}
				std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
				level++;
			}), 0);
		std::cout<<"-- end ast --\n\n";
	}
}





