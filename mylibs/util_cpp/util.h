#include <string>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>            
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <regex>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/optional.hpp>




namespace and_util {
	
	//tree
	template <typename Node, typename... Info>
	void walk(const Node& n, void (*cb)(const Node&, Info&...), const Info&... info) {
		auto copy_args = [&cb, &n](Info... _info) {
			cb(n, _info...);
			std::for_each(n.begin(), n.end(), [&cb, &_info...](const Node& _n) { walk(_n, cb, _info...);  });
		};
		copy_args(info...);
	}
	
	template <typename Node, typename... Info>
	void walk(const Node& n, std::function<void(const Node&, Info&...)> cb, const Info&... info) {
		auto copy_args = [&cb, &n](Info... _info) {
			cb(n, _info...);
			std::for_each(n.begin(), n.end(), [&cb, &_info...](const Node& _n) { walk(_n, cb, _info...);  });
		};
		copy_args(info...);
	}
	
	template <typename Node, typename... Info>
	void walk(const Node& n, std::function<bool(const Node&, Info&...)> cb, const Info&... info) {
		auto copy_args = [&cb, &n](Info... _info) {
			if (cb(n, _info...))
				std::for_each(n.begin(), n.end(), [&cb, &_info...](const Node& _n) { walk(_n, cb, _info...);  });
		};
		copy_args(info...);
	}
	
	template <typename Node, typename... Info>
	void walk_mut(Node& n, std::function<void(Node&, Info&...)> cb, const Info&... info) {
		auto copy_args = [&cb, &n](Info... _info) {
			cb(n, _info...);
			std::for_each(n.begin(), n.end(), [&cb, &_info...](Node& _n) { walk(_n, cb, _info...);  });
		};
		copy_args(info...);
	}
	
	


	// uid
	auto generate_uid() -> std::string {
		boost::uuids::uuid uuid = boost::uuids::random_generator()();
		return boost::lexical_cast<std::string>(uuid);
	}




	// regex
	bool regex_match(const char * regex, const std::string& s) {
		std::regex r(regex, std::regex_constants::ECMAScript);
		return std::regex_match(s, r);
	}


	// why: traverse variadic params..
	namespace {
		template <typename It, typename T>
		void _spread_nth(const It& it, T& first) {
			first = *it;
		}

		template <typename It, typename T, typename... V>
		void _spread_nth(const It& it, T& first, V&... rest) {
			_spread_nth(it, first);
			_spread_nth(it+1, rest...);
		}
	}

	// int a, b
	// vector[2] c
	// spread(c.begin, a, b)
	template <typename It, typename... V>
	void spread(const It& it, V&... v) {

		_spread_nth(it, v...);

		// how overload lambda?
		//auto set_nth = [&vec](auto&& self, int idx, const auto& first, const auto&... rest){
			//if (sizeof...(rest) == 1){
				////first = vec[idx];
			//} else {
				//self(self, idx+1, 1);
			//}
		//};
		//set_nth(set_nth, 0, 1, 2);
	}

	template <typename Iterator, typename... String>
	void _regex(char const * regex, const Iterator& begin, const Iterator& end, bool return_whole, String&... m) {
		std::regex r(regex, std::regex_constants::ECMAScript);
		auto it = std::sregex_iterator(begin, end, r);
		int offset = return_whole ? 0 : 1;
		for (std::sregex_iterator i = it; i != std::sregex_iterator(); ++i){
			std::smatch _m = *i;
			if (_m.size() > offset)
				spread(_m.cbegin()+offset, m...);
			break;
		}
	}
	
	template <typename Iterator, typename... String>
	void regex_first_match_groups(char const * regex, const Iterator& begin, const Iterator& end, String&... m) {
		_regex(regex, begin, end, false, m...);
	}
	
	template <typename Iterator, typename... String>
	void regex_first_match_w_groups(char const * regex, const Iterator& begin, const Iterator& end, String&... m) {
		_regex(regex, begin, end, true, m...);
	}
	
	template <typename... String>
	void regex_first_match_groups(char const * regex, const std::string& s, String&... m) {
		_regex(regex, s.begin(), s.end(), false, m...);
	}
	
	template <typename... String>
	void regex_first_match_w_groups(char const * regex, const std::string& s, String&... m) {
		_regex(regex, s.begin(), s.end(), true, m...);
	}

	//template <typename... String>
	//void regex_first_match_groups(char const * regex, const std::string& s, String&... m) {
		//std::regex r(regex, std::regex_constants::ECMAScript);
		//auto it = std::sregex_iterator(s.begin(), s.end(), r);
		//for (std::sregex_iterator i = it; i != std::sregex_iterator(); ++i){
			//std::smatch _m = *i;
			//if (_m.size() > 1)
				//spread(_m.cbegin()+1, m...);
			//break;
		//}
	//}
	
	//template <typename... String>
	//void regex_first_match_w_groups(char const * regex, const std::string& s, String&... m) {
		//std::regex r(regex, std::regex_constants::ECMAScript);
		//auto it = std::sregex_iterator(s.begin(), s.end(), r);
		//for (std::sregex_iterator i = it; i != std::sregex_iterator(); ++i){
			//std::smatch _m = *i;
			//if (_m.size() > 0)
				//spread(_m.cbegin(), m...);
			//break;
		//}
	//}
	
	//template <typename Iterator>
	//void regex_match_1(const char * regex, const Iterator & begin, const Iterator & end, 
			//std::string & m1, std::string * whole_match = nullptr) {
		//std::regex r(regex, std::regex_constants::ECMAScript);
		//auto it = std::sregex_iterator(begin, end, r);
		//for (std::sregex_iterator i = it; i != std::sregex_iterator(); ++i){
			//std::smatch m = *i;
			//if (whole_match && m.size() > 0)
				//*whole_match = m[0];
			//if (m.size() > 1)
				//m1 = m[1];
		//}
	//}






	
	// string
	// http://www.boost.org/doc/libs/1_55_0/doc/html/string_algo/quickref.html#idp206855960


	// scrape
	auto inner_delim_safe(const std::string & s, const std::string & open, const std::string & close) -> std::string 
		//es. A aa A bb B cc B -> aa A bb B cc
		//assume open != close
		//empty string if not match, or bad formed input (tags not matching, empty)
	{

		if (s.empty())
			goto bad_formed;

		{
		auto i = s.find(open);
		if (i == std::string::npos)
			goto bad_formed;
		auto start_i = i;

		auto j = start_i + 1;
		int count = 1;
		std::string tmp = s.substr(j);
		while (count != 0) {
			auto j1 = tmp.find(open);
			auto j2 = tmp.find(close);
			if (j1 != std::string::npos && j1 < j2){
				count++;
				j += j1 + 1;
				tmp = tmp.substr(j1+1);
				continue;
			} else {
				if (j2 != std::string::npos){
					count--;
					if (count < 0) goto bad_formed;
					j += j2 + 1;
					tmp = tmp.substr(j2+1);
					continue;
				} else {
					goto bad_formed;
				}
			}
		}
		j--;

		return s.substr(start_i + open.size(), j - open.size()-start_i);
		}
bad_formed:
		return std::string();
	}



	// file
	auto get_file_contents(const char *filename) -> std::string
	{
	  std::ifstream in(filename, std::ios::in | std::ios::binary);
	  if (in) {
		std::ostringstream contents;
		contents << in.rdbuf();
		in.close();
		return(contents.str());
	  }
	  throw(errno);
	}


	auto get_stdin() -> std::string {
		std::cin >> std::noskipws;
		std::istream_iterator<char> it(std::cin);
		std::istream_iterator<char> end;
		std::string results(it, end);
		return results;
	}


	// shell
	auto exec(const char* cmd) -> std::string {
		FILE* pipe = popen(cmd, "r");
		if (!pipe) throw(errno);
		char buffer[128];
		std::string result = "";
		while (fgets(buffer, 128, pipe) != NULL) {
			result += buffer;
		}
		pclose(pipe);
		return result;
	}	
}




