
#include <boost/variant.hpp>
#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <vector>

#include <cassert>

#include "mylibs/util_cpp/util.h"
#include "mylibs/util_cpp/spirit.h"



#define TARGET_EXTENSION ".uc1"
#define TARGET_PREFIX    "§"


bool debug = false;
bool shell_enabled = true;

using namespace std::string_literals;


//template <typename Ast>
//void print_ast(const Ast& root, const std::string& name = "") {
	//std::cout<<"-- ast "<< name << " --\n";
	//and_util::walk(root, 
		//static_cast<void (*)(const Ast&, int&)>(
		//[](const Ast& n, int& level){
			//for (int i=0; i<level; ++i) {
				//std::cout<<"  ";
			//}
			//std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			//level++;
		//}), 0);
	//std::cout<<"-- end ast --\n\n";
//}




namespace templat {
	
	using _map = boost::make_recursive_variant<
		   std::string, 
		   std::map<std::string, boost::recursive_variant_ >
		>::type;
	//using map = std::map<std::string, std::pair<std::string, _map>>; // key(string) => (type(string), value(variant))
	using map = std::map<std::string, _map>; 
}

void puppet_generate_hash(const ::templat::map& m, std::string& out);






namespace dsl {

namespace uc {
	using namespace and_spirit;


	enum class NodeType { DEFAULT, MAP, KEY, VALUE, };
	using ast = and_spirit::ast<NodeType>;


	//ast: (key->value) | map -> (map | (key->value))

	template <typename Iterator>
	struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){

			value = lexeme[(+(char_ - eol))[emit<NodeType, NodeType::VALUE>]];

			key = (+(char_ - eol - '=' - '}' - '{'))[emit<NodeType, NodeType::KEY>] >> '=' >> value[push<ast>];

			map = lexeme[(+(char_ - space - eol - '{'))[emit<NodeType, NodeType::MAP>]] >> '{' >> *(key[push<ast>] | map[push<ast>]) >> '}';

			root = *( key[push<ast>] |  map[push<ast>]  );
		
		}
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root, map, key, value;
		//boost::spirit::qi::rule<Iterator, ast()> root;
	};
}

/*
 * Soci {
 * 		codice_socio: string, not empty, UNIQUE 
 * 		codice_fiscale: string, not empty if codice_socio = 666
 *
 * 		ass: Ass, not empty
 *
 * 		circolo: Circoli(ass.circolo)
 *
 * 		UNIQUE a, b
 * }
 * */
namespace data {
	using namespace and_spirit;

	const char INDENT[] = "<<INDENT>>\n";
	const char DEDENT[] = "<<DEDENT>>\n";

	enum class NodeType { DEFAULT, ENUM, ENUM_CHOICE, SET, FIELD, FIELD_NAME, CONSTRAINT, TYPE_BUILTIN, TYPE_USER, TYPE_ARRAY, REFINE, COND_IF, CONSTRAINT_INLINE, SET_NAME, FUNC, EXPR_CALL  };
	using ast = and_spirit::ast<NodeType>;


	// ast
	// set -> field -> type_user|type_builtin|type_array, refine->cond_if..., constraint_inline
	// set -> constraint -> field_name
	// type_array -> set_name
	// func ->

	template <typename Iterator>
	struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){

			field_name   = (+(char_ - ':' - '}'))[emit<NodeType, NodeType::FIELD_NAME>]; 	
			type_name    = (+(char_ - ':' - '}' - '('))[emit<NodeType, NodeType::DEFAULT>]; 
			type_builtin = lexeme[(+(char_ - eol - '}' - ','))[emit<NodeType, NodeType::TYPE_BUILTIN>]];
			//type_user = 
			type_array   = type_name[copy_name<ast, NodeType, NodeType::TYPE_ARRAY>] >> '(' >> set_name[push<ast>] >> ')'; 
			cond_if      = lit("if ") >> lexeme[(+(char_ - ',' - '}'))[emit<NodeType, NodeType::COND_IF>]];
			refine       = lexeme[(+(char_ - eol - '}' - ',' - "if"))[emit<NodeType, NodeType::REFINE>]] >> -cond_if[push<ast>];
			constraint_inline = string("UNIQUE")[emit_str<NodeType, NodeType::CONSTRAINT_INLINE>];
			type         = (type_builtin[push<ast>] | type_user[push<ast>] | type_array[push<ast>]) 
							>> *(',' >> refine[push<ast>]) >> -(',' >> constraint_inline[push<ast>]);
			field        = field_name[copy_name<ast, NodeType, NodeType::FIELD>] >> ':' >> type[copy_children<ast>];
			constraint   = string("UNIQUE")[emit_str<NodeType, NodeType::CONSTRAINT>] >> field_name[push<ast>] >> *(',' >> field_name[push<ast>]);
			set_name     = (+(char_ - '{'))[emit<NodeType, NodeType::SET_NAME>];;
			set          = set_name[copy_name<ast, NodeType, NodeType::SET>] >> '{' >> *field[push<ast>] >> *constraint[push<ast>] >> '}';
			enum_choice  = field_name[copy_name<ast, NodeType, NodeType::ENUM_CHOICE>];
			enumm        = lit("enum") >> set_name[copy_name<ast, NodeType, NodeType::ENUM>] >> '{' >> *enum_choice[push<ast>] >> '}';


			//variable  = (+(char_ - ':' - '}'))[emit<NodeType, NodeType::>]; 	
			//func_name = (+(char_ - ':'))[emit<NodeType, NodeType::FUNC>];
			//expr_call = func_name[copy_name<ast, NodeType, NodeType::EXPR_CALL>];
			//expr_if   = lit("if") >> lit(INDENT) >> (expr_if_custom | ) >> -expr_else >> lit(DEDENT);
			//expr_if_custom = lexeme[(+(char_ - '\n'))[emit<>]];
			//expr_if_empty = variable >> lit("is empty"); 
			//expr_get
			//expr_put
			//expr_new
			//expr_ok
			//expr_fail
			////expr_get  = ;
			////expr_put  =;
			//expr      = expr_call[copy_data<ast>];
			//func      = func_name[copy_data<ast>] >> ':' >> lit(INDENT) >> *expr[push<ast>] >> lit(DEDENT);


			root = *set[push<ast>];
		
		}
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root, set, field, field_name, type, type_builtin, type_user, refine, constraint_inline, constraint, cond_if, enumm, enum_choice, set_name, type_array, type_name, func, func_name, expr, expr_call;
		//boost::spirit::qi::rule<Iterator, ast()> root;
	};
}
}
BOOST_FUSION_ADAPT_STRUCT(
		dsl::uc::ast,
		(std::string, name)
		(std::vector<dsl::uc::ast>, children)
		(dsl::uc::NodeType, type)
)
BOOST_FUSION_ADAPT_STRUCT(
		dsl::data::ast,
		(std::string, name)
		(std::vector<dsl::data::ast>, children)
		(dsl::data::NodeType, type)
)






/*
 * §for set
 * create table §set if not exists (
 * 	...
 * 	§for field in set
 * 	§field ... §set.field ... §field.type NOT NULL //§field -> key  §set.field -> value
 * 	§endfor
 * )
 * §endfor
 *
 * §(echo ciao)
 *
 * */
namespace templat {

#define TEMPLAT_FOR    "for"
#define TEMPLAT_ENDFOR "endfor"
#define TEMPLAT_IF     "if"
#define TEMPLAT_IFNOT  "if not" //TODO: remove, use boolean expression
#define TEMPLAT_ENDIF  "endif"
#define TEMPLAT_PRINT_PUPPET "print_puppet"

	
	using _map = boost::make_recursive_variant<
		   std::string, 
		   std::map<std::string, boost::recursive_variant_ >
		>::type;
	//using map = std::map<std::string, std::pair<std::string, _map>>; // key(string) => (type(string), value(variant))
	using map = std::map<std::string, _map>; 


	void print_map(const map& m, std::ostream& out = std::cout, const char* header = "", const char* prologue = "", const char * duepunti = ": ", const char * virgola = "", int level = 0) {
		if (level == 0 )
			out<< header;
		int j = 0;
		for (const auto& it : m){
			for (int i=0; i<level; ++i) {  out<<"  "; }
			
			_map const * _m = &it.second;
			std::string const * s_val = boost::get<std::string>(_m);
			if (s_val){
				if (j > 0) out << virgola;
				out << it.first << duepunti << '\'' << *s_val << '\'' <<"\n";
			} else {
				if (j > 0) out << virgola;
				out << it.first << duepunti << " {\n";
				
				map const * m_val = boost::get<map>(_m);
				print_map(*m_val, out, header, prologue, duepunti, virgola, level+1);
				
				for (int i=0; i<level; ++i) { out<<"  "; }
				out << "}" << "\n";
			}
			j++;
		}
		if (level == 0 )
			out<< prologue;
	}


	// ast: value_com -> value_com -> ... | for_sym -> (value_com->value_com->..)?   |  if -> if_left -> value_path, if -> (if_right)?
	
	namespace parser {
		using namespace and_spirit;

		enum class NodeType { DEFAULT, VALUE_COMPONENT, FOR_SYM, IF, IFNOT, IF_LIT, IF_LEFT, IF_RIGHT, };
		using ast = and_spirit::ast<NodeType>;

		
		template <typename Iterator>
		struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
		//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
			grammar():grammar::base_type(root){

				value_component = (+(char_("a-zA-Z0-9_") - '.'))[emit<NodeType, NodeType::VALUE_COMPONENT>];
				value_path = value_component[copy_data<ast>] >>  -(lit(".") >> value_path[push<ast>]);

				for_sym = lexeme[(+(char_ - space - eol))[emit<NodeType, NodeType::FOR_SYM>]];
				for_in = value_path[_val = _1];

				if_lit = (+(char_))[emit<NodeType, NodeType::IF_LIT>];
				if_left = value_path[copy_set_type<ast, NodeType, NodeType::IF_LEFT>];
				if_right = if_lit[_val = _1];

				root = ( lit(TARGET_PREFIX TEMPLAT_FOR) >> for_sym[_val = _1] >> -(lit("in") >> for_in[push<ast>]) )
					| ( string(TARGET_PREFIX TEMPLAT_IFNOT)[emit_str<NodeType, NodeType::IFNOT>] >> if_left[push<ast>] >> -(lit("=") >> if_right[push<ast>]) )
					| ( string(TARGET_PREFIX TEMPLAT_IF)[emit_str<NodeType, NodeType::IF>] >> if_left[push<ast>] >> -(lit("=") >> if_right[push<ast>]) )
					| ( lit(TARGET_PREFIX) >> value_path[_val = _1] ); 

			}
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root, value_path, value_component, for_sym, for_in, if_lit, if_left, if_right;
			//boost::spirit::qi::rule<Iterator, ast()> root;
		};

	}



	using tgrammar = parser::grammar<std::string::iterator>;

	using symbol_entry = std::pair<std::string, map const * const>;//symbol name -> key relative to map
	using symbol_table = std::map<std::string, symbol_entry>;

	struct context {
		symbol_table& t;
		const map& m;
		const map& alt_m;
	};
	

	//rule: x.y (or.y(not implemented)) -> value; no dot -> key
		template <typename Val, typename K>
		void _key(Val const *& val, const K& k ) {}
		template <>
		void _key<std::string, std::string>(std::string const *& val, const std::string& k ) {
			val = &k;
		}
	// return pointer to ast/t/m...
	template <typename Val>
	auto _val(const parser::ast& ast, const context& ctx, const bool can_return_key = true, bool use_alt_map = false) -> Val const* {
		const auto& m = use_alt_map ? ctx.alt_m : ctx.m;
		//const auto&m = ctx.m;
		auto& t = ctx.t;

		auto const * cur = &m;
		bool found = true;
		Val const * val = nullptr;
		bool has_parent = false;
		and_util::walk(ast, static_cast<std::function<void(const parser::ast&)>>([&](const parser::ast& n ){

				if (!found) return;

				if (!n.children.empty()){
					if (t.find(n.name) != t.end()){
						auto s_entry = t.at(n.name);
						//cur = &boost::get<map>((s_entry.second)->at(s_entry.first));
						map const * _map = boost::get<map>(&( (s_entry.second)->at(s_entry.first) ));
						
						if (has_parent){
							// caso a.symbol where symbol is a map
							_map = boost::get<map>(&(cur->at(s_entry.first)));
						}

						if (_map) {
							cur = _map;
						} else {
							found = false;
							if (debug) std::cout<<"not found 0: "<<n.name<<"[end]\n";
						}
					} else {
						if (cur->find(n.name) != cur->end()) {
							cur = &boost::get<map>(cur->at(n.name));
						} else {
							found = false;
							if (debug) std::cout<<"not found 1: "<<n.name<<"[end]\n";
						}
					}
					has_parent = true;
				} else {
					if (cur == &m && can_return_key) {
						if (t.find(n.name) != t.end()){
							auto s_entry = t.at(n.name);
							_key(val, s_entry.first);
						} else if (cur->find(n.name) != cur->end()) {
							Val const * _val = boost::get<Val>(&(cur->at(n.name)));
							if (_val)
								val = _val;
							else{
								_key(val, n.name);
							}
						} else {
							found = false;
							if (debug) std::cout<<"not found 2: "<<n.name<<"[end]\n";
						}
					} else {
						if (t.find(n.name) != t.end()){
							auto s_entry = t.at(n.name);
							Val const * _val;
							
							if (has_parent){
								// caso a.symbol where symbol is a map
								_val = boost::get<Val>(&(cur->at(s_entry.first)));
							} else {
								_val = boost::get<Val>(&((s_entry.second)->at(s_entry.first)));
							}
							//Val const * _val = boost::get<Val>(&((s_entry.second)->at(s_entry.first)));
							if (_val)
								val = _val;
							else if (can_return_key)
								_key(val, n.name);
							else {
								found = false;
								if (debug) std::cout<<"not found 3: "<<n.name<<"[end]\n";
							}
						} else {
							if (cur->find(n.name) != cur->end()){
								Val const * _val = boost::get<Val>(&(cur->at(n.name)));
								if (_val){
									val = _val;
								}else if (can_return_key){
									_key(val, n.name);
								} else {
									found = false;
									if (debug) std::cout<<"not found 4: "<<n.name<<"[end]\n";
								}
							} else {
								//alt..
								if (use_alt_map && cur->find("0") != cur->end()){
									map const * _map = boost::get<map>(&( (cur)->at("0") ));
									if (_map) {
										cur = _map;
										if (cur->find(n.name) != cur->end()){
											Val const * _val = boost::get<Val>(&(cur->at(n.name)));
											if (_val){
												val = _val;
											}else if (can_return_key){
												_key(val, n.name);
											} else {
												found = false;
												if (debug) std::cout<<"not found 5b: "<<n.name<<"[end]\n";
											}
										} else {
											found = false;
											if (debug) std::cout<<"not found 5c: "<<n.name<<"[end]\n";
										}
									} else {
										found = false;
										if (debug) std::cout<<"not found 5d: "<<n.name<<"[end]\n";
									}
								} else {
									found = false;
									if (debug) std::cout<<"not found 5a: "<<n.name<<"[end]\n";
								}
							}
						}
					}
				}
		}));
		//if (!found)
			//throw std::runtime_error("val not found"); 
		return val;
	}

	


	bool _apply_val(std::string & s, const context& ctx, bool empty_if_not_found = true);
	bool _apply_for(std::string & inner, const std::string& header, const context& ctx, bool use_alt_map = false);
	bool _apply_if(std::string & inner, const std::string& header, const context& ctx, bool ifnot = false);

	
	bool apply(std::string & content, const context& ctx, bool empty_if_not_found = true) {
		int i;
		int retcode;

#define _SIZE(x) (sizeof(x)-1)
#define _ERROR 2
#define _OK 1
#define _SKIP 0

		auto _BLOCK = [&content, &ctx, &i](const auto& TAG_START, const auto& TAG_END, const auto F, bool opt, bool ALT_MAP = false){
			if (content.compare(i+_SIZE(TARGET_PREFIX), _SIZE(TAG_START), TAG_START) == 0) {
				auto it = std::find(content.begin()+i, content.end(), '\n');
				if (it == content.end()){
					if (debug) std::cout<<"for malformed\n";
					return _ERROR;
				}
				const auto _header_without_prefix = content.substr(i+_SIZE(TARGET_PREFIX)+_SIZE(TAG_START), it-(content.begin()+i+_SIZE(TARGET_PREFIX)+_SIZE(TAG_START))+1);//newline included
				std::string inner = and_util::inner_delim_safe(content, ""s + TARGET_PREFIX + TAG_START, ""s TARGET_PREFIX + TAG_END); 
				boost::algorithm::replace_first(inner, _header_without_prefix, "");
				auto v = inner;
				const auto _header = ""s + TARGET_PREFIX + TAG_START + _header_without_prefix;
				if (!F(v,_header, ctx, opt)){
					if (debug) std::cout<<"\ntrying alt_map..\n\n";
					if (ALT_MAP && !F(v, _header, ctx, true)){
						if (debug) std::cout<<"for error\n";
						return _ERROR;
					}
				}
				boost::algorithm::replace_first(content, _header+inner+ TARGET_PREFIX + TAG_END, v);
				return _OK;
			}
			return _SKIP;
		};


		while (true) {
			std::string regex_m1, regex_m2, regex_w;

			i = content.find(TARGET_PREFIX);
			if (i == std::string::npos) // end
				return true;

			// print_puppet
			if (content.compare(i+_SIZE(TARGET_PREFIX), _SIZE(TEMPLAT_PRINT_PUPPET), TEMPLAT_PRINT_PUPPET) == 0) {
				std::string s;
				puppet_generate_hash(ctx.m, s);
				boost::algorithm::replace_first(content, TEMPLAT_PRINT_PUPPET, s);
				continue;
			}

			// for
			retcode = _BLOCK(TEMPLAT_FOR, TEMPLAT_ENDFOR, _apply_for, false, true);
			if (retcode == _ERROR) return false; else if (retcode == _OK) continue;
			/*if (content.compare(i+_SIZE(TARGET_PREFIX), _SIZE(TEMPLAT_FOR), TEMPLAT_FOR) == 0) {
				auto it = std::find(content.begin()+i, content.end(), '\n');
				if (it == content.end()){
					if (debug) std::cout<<"for malformed\n";
					return false;
				}
				const auto _header_without_prefix = content.substr(i+_SIZE(TARGET_PREFIX)+_SIZE(TEMPLAT_FOR), it-(content.begin()+i+_SIZE(TARGET_PREFIX)+_SIZE(TEMPLAT_FOR))+1);//newline included
				std::string inner = and_util::inner_delim_safe(content, "" TARGET_PREFIX TEMPLAT_FOR, "" TARGET_PREFIX TEMPLAT_ENDFOR ""); 
				boost::algorithm::replace_first(inner, _header_without_prefix, "");
				auto v = inner;
				const auto _header = TARGET_PREFIX TEMPLAT_FOR + _header_without_prefix;
				if (!_apply_for(v,_header, ctx)){
					if (debug) std::cout<<"\ntrying alt_map..\n\n";
					if (!_apply_for(v, _header, ctx, true)){
						if (debug) std::cout<<"for error\n";
						return false;
					}
				}
				boost::algorithm::replace_first(content, _header+inner+ "" TARGET_PREFIX TEMPLAT_ENDFOR "", v);
				continue;
			}*/


			// if
			retcode = _BLOCK(TEMPLAT_IFNOT " ", TEMPLAT_ENDIF, _apply_if, true, false);
			if (retcode == _ERROR) return false; else if (retcode == _OK) continue;
			retcode = _BLOCK(TEMPLAT_IF " ", TEMPLAT_ENDIF, _apply_if, false, false);
			if (retcode == _ERROR) return false; else if (retcode == _OK) continue;

			
			// shell
			and_util::regex_first_match_w_groups(TARGET_PREFIX "([a-zA-Z]*?)\\((.*?)\\)\\1", content.begin()+i, content.end(), regex_w, regex_m2, regex_m1);
			if (!regex_m1.empty()){
				auto cmd_result = ""s;
				if (shell_enabled)
					cmd_result = and_util::exec(regex_m1.c_str());
				boost::algorithm::replace_first(content, regex_w, cmd_result); 
				continue;
			}


			// val
			//and_util::regex_match_1("(" TARGET_PREFIX "\\S+)", line.begin(), line.end(), regex_m1);
			and_util::regex_first_match_groups("(" TARGET_PREFIX "[a-zA-Z0-9_.]+)", content.begin()+i, content.end(), regex_m1);
			if (!regex_m1.empty()) {
				std::string v = regex_m1;
				if (!_apply_val(v, ctx, empty_if_not_found)){
					if (empty_if_not_found)
						boost::algorithm::replace_first(content, TARGET_PREFIX, "__(/[[no found]]))__");
					else
						return false;
				} else {
					boost::algorithm::replace_first(content, regex_m1, v);
				}
				continue;
			}
		}
		return true;
	}

	/*bool apply(std::string & content, const context& ctx, bool empty_if_not_found = true) { 
		while (true) {
			std::string line;
			std::istringstream iss(content);
			std::string regex_m1, regex_m2, regex_w;
			while (std::getline(iss, line)) {
				// print_puppet
				and_util::regex_first_match_groups("(" TARGET_PREFIX TEMPLAT_PRINT_PUPPET ")", line, regex_m1);
				if (!regex_m1.empty()) {
					std::string s;
					puppet_generate_hash(ctx.m, s);
					boost::algorithm::replace_first(content, regex_m1, s);
					goto repeat;
				}

				// for
				and_util::regex_first_match_groups("(" TARGET_PREFIX TEMPLAT_FOR " [^\\n]+)", line, regex_m1);
				if (!regex_m1.empty()) {
					regex_m1.append("\n");

					const auto _header_without_prefix = boost::algorithm::replace_first_copy(regex_m1, "" TARGET_PREFIX TEMPLAT_FOR, "");
					std::string inner = and_util::inner_delim_safe(content, "" TARGET_PREFIX TEMPLAT_FOR, "" TARGET_PREFIX TEMPLAT_ENDFOR ""); 
					boost::algorithm::replace_first(inner, _header_without_prefix, "");
					
					//if (inner.empty())//todo: errors...
					auto v = inner;
					if (!_apply_for(v, regex_m1, ctx)){
						if (debug) std::cout<<"\ntrying alt_map..\n\n";
						if (!_apply_for(v, regex_m1, ctx, true)){
							goto err;
						}
					}
					boost::algorithm::replace_first(content, regex_m1+inner+ "" TARGET_PREFIX TEMPLAT_ENDFOR "", v);
					goto repeat;
				}
				
				// if
				and_util::regex_first_match_groups("(" TARGET_PREFIX TEMPLAT_IF " [^\\n]+)", line, regex_m1);
				and_util::regex_first_match_groups("(" TARGET_PREFIX TEMPLAT_IFNOT " [^\\n]+)", line, regex_m2);
				if (!regex_m2.empty() ){
					regex_m2.append("\n");

					const auto _header_without_prefix = boost::algorithm::replace_first_copy(regex_m2, "" TARGET_PREFIX TEMPLAT_IFNOT, "");
					std::string inner = and_util::inner_delim_safe(content, "" TARGET_PREFIX TEMPLAT_IFNOT, "" TARGET_PREFIX TEMPLAT_ENDIF ""); 
					boost::algorithm::replace_first(inner, _header_without_prefix, "");
					
					auto v = inner;
					if (!_apply_if(v, regex_m2, ctx, true))
						goto err;
					boost::algorithm::replace_first(content, regex_m2+inner+ "" TARGET_PREFIX TEMPLAT_ENDIF "", v);
					goto repeat;
				} else if (!regex_m1.empty() ){
					regex_m1.append("\n");

					const auto _header_without_prefix = boost::algorithm::replace_first_copy(regex_m1, "" TARGET_PREFIX TEMPLAT_IF, "");
					std::string inner = and_util::inner_delim_safe(content, "" TARGET_PREFIX TEMPLAT_IF, "" TARGET_PREFIX TEMPLAT_ENDIF ""); 
					boost::algorithm::replace_first(inner, _header_without_prefix, "");
					
					auto v = inner;
					if (!_apply_if(v, regex_m1, ctx))
						goto err;
					boost::algorithm::replace_first(content, regex_m1+inner+ "" TARGET_PREFIX TEMPLAT_ENDIF "", v);
					goto repeat;
				}

				// shell
				and_util::regex_first_match_w_groups(TARGET_PREFIX "([a-zA-Z]*?)\\((.*?)\\)\\1", line, regex_w, regex_m2, regex_m1);
				if (!regex_m1.empty()){
					auto cmd_result = ""s;
					if (shell_enabled)
						cmd_result = and_util::exec(regex_m1.c_str());
					boost::algorithm::replace_first(content, regex_w, cmd_result); 
					goto repeat;
				}
				
				// val
				//and_util::regex_match_1("(" TARGET_PREFIX "\\S+)", line.begin(), line.end(), regex_m1);
				and_util::regex_first_match_groups("(" TARGET_PREFIX "[a-zA-Z0-9_.]+)", line, regex_m1);
				if (!regex_m1.empty()) {
					std::string v = regex_m1;
					if (!_apply_val(v, ctx, empty_if_not_found)){
						if (empty_if_not_found)
							boost::algorithm::replace_first(content, TARGET_PREFIX, "__(/[[no found]]))__");
						else
							goto err;
					} else {
						boost::algorithm::replace_first(content, regex_m1, v);
					}
					goto repeat;	
				}
			}
done:
			break;
repeat:
			continue;
		} 
		if (empty_if_not_found)
			boost::algorithm::replace_all(content, "__(/[[no found]]))__", TARGET_PREFIX); 
		return true;
err:
		return false;
	}*/
	
	bool _apply_val(std::string & s, const context& ctx, bool empty_if_not_found) {
		tgrammar g;
		parser::ast root;
		bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;

		if (debug){
			std::cout<<"resolving "<<s<<"\n";
			//for ( const auto& p : m) std::cout<<p.first<<",";std::cout<<"\n";
			//for ( const auto& p : t) std::cout<<p.first<<",";std::cout<<"\n";
		}
		std::string const * _s = _val<std::string>(root, ctx);
		if (!_s){
			if (empty_if_not_found)
				s = "";
			else
				return false;
		} else
			s = *_s;

		if (debug){
			std::cout<<"resolved to "<<s<<"\n";
		}
		return true;
	}

	bool _apply_if(std::string & inner, const std::string& header, const context& ctx, bool ifnot) {
		tgrammar g;
		parser::ast root;
		auto h = const_cast<std::string&>(header);
		bool r = boost::spirit::qi::phrase_parse(h.begin(), h.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;

		//get val for if_left, if_right
		if (debug){
			std::cout<<"if: resolving "<<header<<"\n";
			//for ( const auto& p : m) std::cout<<p.first<<",";std::cout<<"\n";
			//for ( const auto& p : t) std::cout<<p.first<<",";std::cout<<"\n";
		}
		std::string const * left_val = _val<std::string>(root.children[0], ctx);
		if (debug){
			std::cout<<"if: resolved left to "<<(left_val?*left_val:"")<<"\n";
		}
		std::string const * right_val = nullptr;
		if (root.children.size() > 1){
			right_val = &(root.children[1].name);
		}

		//compare
		bool cond;
		if (!left_val){
			cond = false;
		} else if (!right_val){
			if (!left_val){
				cond = false;
			} else {
				cond = true;
			}
		} else {
			if (*left_val == *right_val){
				cond = true;
			} else {
				cond = false;
			}		
		}
		if (ifnot)
			cond = !cond;
		if (cond){
			if (!apply(inner, ctx))
				return false;
		} else {
			inner = "";
		}


		return true;
	}

	bool _apply_for(std::string & inner, const std::string& header, const context& ctx, bool use_alt_map) {
		tgrammar g;
		parser::ast root;
		auto h = const_cast<std::string&>(header);
		bool r = boost::spirit::qi::phrase_parse(h.begin(), h.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;


		//get map
		if (debug){
			std::cout<<"for: resolving "<<header<<"\n";
		}
		map const * cur = nullptr;
		if (!root.children.empty()) {//es. §for sym in ...
			auto & n = root.children[0];
			cur = _val<map>(n, ctx, false, use_alt_map);
			if (cur == nullptr && !use_alt_map){
				if (debug)
					std::cout<<"not resolved\n";
				return false;
			} else if (cur == nullptr) {
				if (debug)
					std::cout<<"resolved to <empty>\n";
				inner = "";
				return true;
			}

		}
		if (cur == nullptr)
			cur = &ctx.m;

		
		std::string s;
		const auto _inner = inner;

		for (const auto& kv : *cur){
			const auto & k = kv.first;

			//add symbol
			ctx.t.emplace(root.name, symbol_entry(k,cur));


			if (!apply(inner, ctx, use_alt_map)) {
				//remove symbol
				ctx.t.erase(root.name);
				return false;
			}

			s.append(inner);
			inner = _inner;
			

			//remove symbol
			ctx.t.erase(root.name);
		}


		inner = s;

		return true;
	}
	



}
BOOST_FUSION_ADAPT_STRUCT(
		templat::parser::ast,
		(std::string, name)
		(std::vector<templat::parser::ast>, children)
		(templat::parser::NodeType, type)
)








// organize...	
	
void puppet_generate_hash(const ::templat::map& m, std::string& out) {
	std::stringstream ss;
	::templat::print_map(m, ss, "{\n", "\n}", "=> ", ",");
	out = ss.str();
}






//dsl ast -> templat, ...
namespace backend {

	namespace uc {
		void template_map(const ::dsl::uc::ast& root, ::templat::map& m)  {
			and_util::walk(root, static_cast<std::function<void(const ::dsl::uc::ast&, ::templat::map*&)>>([&](const ::dsl::uc::ast& n, ::templat::map*& cur){
					if (n.type == ::dsl::uc::NodeType::DEFAULT || n.name.empty()){
						
					} else if (n.type == ::dsl::uc::NodeType::MAP) {
						if (cur->find(n.name) == cur->end()){
							(*cur)[n.name] = ::templat::map();
							cur = boost::get<::templat::map>(&((*cur)[n.name]));
						} else {
							auto& _tmp = boost::get<::templat::map>((*cur)[n.name]);
							if (_tmp.find("0") == _tmp.end()){
								const auto tmp = _tmp;
								cur->erase(cur->find(n.name));
								::templat::map _new { 
									{"0", std::move(tmp)},
									{"1", ::templat::map()},
								};
								(*cur)[n.name] = std::move(_new);
								auto _cur = boost::get<::templat::map>(&((*cur)[n.name]));
								cur = boost::get<::templat::map>(&((*_cur)["1"]));
							} else {
								auto i = _tmp.size();
								_tmp[std::to_string(i)] = ::templat::map();
								cur = boost::get<::templat::map>(&(_tmp.at(std::to_string(i))));
							}
						}
					} else if (n.type == ::dsl::uc::NodeType::KEY) {
						(*cur)[n.name] = n.children[0].name;
					}
			}),&m);
		}
		
		// every map is array..
		void template_alt_map(const ::dsl::uc::ast& root, ::templat::map& m)  {
			and_util::walk(root, static_cast<std::function<void(const ::dsl::uc::ast&, ::templat::map*&)>>([&](const ::dsl::uc::ast& n, ::templat::map*& cur){
					if (n.type == ::dsl::uc::NodeType::DEFAULT || n.name.empty()){
						
					} else if (n.type == ::dsl::uc::NodeType::MAP) {
						if (cur->find(n.name) == cur->end()){
							::templat::map _new { 
								{"0", ::templat::map()}
							};
							(*cur)[n.name] = std::move(_new);
							auto _cur = boost::get<::templat::map>(&((*cur)[n.name]));
							cur = boost::get<::templat::map>(&((*_cur)["0"]));
						} else {
							auto& _tmp = boost::get<::templat::map>((*cur)[n.name]);
							auto i = _tmp.size();
							_tmp[std::to_string(i)] = ::templat::map();
							cur = boost::get<::templat::map>(&(_tmp.at(std::to_string(i))));
						}
					} else if (n.type == ::dsl::uc::NodeType::KEY) {
						(*cur)[n.name] = n.children[0].name;
					}
			}),&m);
		}
	}
	

	namespace sql {

		std::map<std::string, std::string> types {{"string", "VARCHAR(200) NOT NULL"}};

		// {"set1": { "constraints": "a__b": { "a":"a", "b":"b" },    "fields": { "field1": { "type": "a" } } }
		void template_map(const ::dsl::data::ast& ast, ::templat::map& map) {
			bool error = false; //todo...
			and_util::walk(ast, static_cast<std::function<void(const ::dsl::data::ast&, ::templat::map*&)>>([](const ::dsl::data::ast& n, ::templat::map*& cur){
				if (n.type == ::dsl::data::NodeType::SET) {	
					::templat::map m;
					m["fields"] = ::templat::map();
					m["constraints"] = ::templat::map();
					(*cur)[n.name] = m;
					cur = boost::get<::templat::map>(&((*cur)[n.name]));
				} else if (n.type == ::dsl::data::NodeType::CONSTRAINT) {
					std::string name;
					::templat::map cc;
					for (auto const & c : n.children) {
						name.append(c.name + "__");
						cc[c.name] = c.name;
					}
					boost::algorithm::erase_last(name, "__");
					auto & ccc = boost::get<::templat::map>((*cur)["constraints"]);
					ccc[name] = cc;
					
				} else if (n.type == ::dsl::data::NodeType::FIELD_NAME) {

				} else if (n.type == ::dsl::data::NodeType::FIELD) {
					::templat::map f;
					auto & n1 = n.children[0];
					assert(n1.type == ::dsl::data::NodeType::TYPE_BUILTIN || n1.type == ::dsl::data::NodeType::TYPE_USER);//todo
					f["type"] = n1.name;
					auto & ff = boost::get<::templat::map>((*cur)["fields"]);
					ff[n.name] = f;
				}
			}), &map);

		}




		//parser for custom sql
		namespace sql_parser {}

		// parse custom sql;
		// generate sql from custom sql ast + dsl ast
		void parse_and_generate_sql(){}
	}
}


//void test(){
	//typedef dsl::data::grammar<std::string::iterator> grammar;
	//grammar g;
	//dsl::data::ast root;
	//std::string s = "Soci { field: string, not empty if a, UNIQUE  }";
	//std::string t;
	//bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
	////bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
	//if (!r){
		//std::cout <<"parse fail\n";
		//return;
	//}

	//int level=0;
	//and_util::walk(root, static_cast<void (*)(const dsl::data::ast&, int&)>([](const dsl::data::ast& n, int& level){
			//std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			//level++;
			//}), level);

	
	//templat::map tmap;
	//backend::sql::template_map(root, tmap);


	//std::string tem = "§for set\n..§set ..\n§endfor";
	//templat::symbol_table st;
	//templat::apply(tem, tmap, st);
	//std::cout<<tem<<"\n";
//}


/*
 * tests:
 * 	a.out --base=a.uc --target=a.txt.uc1 --dsl=uc
 *
 * todo:
 * 	 use parser..
 * 	 debug/source maps..(+breakpoints..)
 *   boolean expression (instead of if not..)
 *   else
 *   define vars/functions
 * */



/*
 * usage: a.out --base=xx.uc --target=aaa --dsl=uc --(debug|test)? --noshell?
 * */





int main(int ac, char* av[]) {
	
	
	namespace po = boost::program_options;
	
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "")
		("base", po::value<std::vector<std::string>>(), "")
		("target", po::value<std::vector<std::string>>(), "")
		("dsl", po::value<std::vector<std::string>>(), "")
		("debug", "write to stdout + debug info")
		("test", "write to stdout")
		("noshell", "do not execute shell commands: §(...)")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);
	
	if (/*!vm.count("base") ||*/ !vm.count("target") || !vm.count("dsl") ) {
		std::cout<<desc<<"\n";
		return 1;
	}
	
	const std::string base   = vm.count("base") ? vm["base"].as< std::vector<std::string> >()[0] : "";
	const std::string target = vm["target"].as< std::vector<std::string> >()[0];
	const std::string dsl    = vm["dsl"].as< std::vector<std::string> >()[0];
	debug                    = vm.count("debug") > 0;
	const bool test          = vm.count("test") > 0;
	shell_enabled            = vm.count("noshell") == 0;


	if (dsl == "uc") {
		dsl::uc::ast root;
		if (!base.empty()){
			typedef dsl::uc::grammar<std::string::iterator> grammar;
			grammar g;
			
			std::string s = and_util::get_file_contents(base.c_str());
			if (debug)
				std::cout<<s<<"\n-------------\n";
			std::string t;
			bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
			//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
			if (!r){
				std::cout <<"parse fail (base)\n";
				return 1;
			}
		}

		if (debug)
			print_ast(root);

		std::string s1 = and_util::get_file_contents(target.c_str());
		if (debug)
			std::cout<<s1<<"\n-------------\n";
	
		templat::map tmap, alt_tmap;
		backend::uc::template_map(root, tmap);
		backend::uc::template_alt_map(root, alt_tmap);


		templat::symbol_table st;
		templat::context ctx { st, tmap, alt_tmap };
		templat::apply(s1, ctx);

		if (debug || test){
			std::cout<<s1;
		} else {
			std::string f = boost::algorithm::replace_first_copy(target, TARGET_EXTENSION, "");
			std::ofstream fout(f);
			fout << s1;
		}
	}

	if (dsl == "data") {
		typedef dsl::data::grammar<std::string::iterator> grammar;
		grammar g;
		dsl::data::ast root;
		std::string s = and_util::get_file_contents(base.c_str());
		std::string indented;
		std::istringstream iss(s);
		and_spirit::indent(iss, indented, dsl::data::INDENT, dsl::data::DEDENT, std::pair<const char*, const char*>("{", "}"));
		std::string t;
		bool r = boost::spirit::qi::phrase_parse(indented.begin(), indented.end(), g, boost::spirit::standard::space, root);
		//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
		if (!r){
			std::cout <<"parse fail (base)\n";
			return 1;
		}

		std::string s1 = and_util::get_file_contents(target.c_str());
	
		templat::map tmap;
		backend::sql::template_map(root, tmap);


		//std::string s1 = "§for set\n..§set ..\n§endfor";
		templat::symbol_table st;
		//templat::apply(s1, tmap, st);


		std::string f = boost::algorithm::replace_first_copy(target, TARGET_EXTENSION, "");
		std::ofstream fout(f);
		fout << s1;
	}


}


