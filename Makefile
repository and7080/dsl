all: help

help:
	@echo "mac: make dsl|dist_linux"
	@echo "linux: ..."

test_spirit:
	clang++ -g -std=c++11 -stdlib=libc++ -I . -I ./boost_1_55_0 test_spirit.cpp -I ./mylibs

test_uc:
	clang++ -g -std=c++11 -stdlib=libc++ -I . -I ./boost_1_55_0 test_uc.cpp -I ./mylibs lib/libboost_program_options.a

test_data:
	clang++ -g -std=c++11 -stdlib=libc++ -I . -I ./boost_1_55_0 test_data.cpp -I ./mylibs lib/libboost_program_options.a

test_data_uc_combined:
	clang++ -g -std=c++11 -stdlib=libc++ -I . -I ./boost_1_55_0 test_data_uc_combined.cpp -I ./mylibs lib/libboost_program_options.a

dsl:
	clang++ -g -std=c++1y -stdlib=libc++ -I . -I /Users/and/bin/boost_1_57_0 dsl.cpp -I ./mylibs lib/libboost_program_options.a

dsl_linux: install_boost_linux
	clang++-3.5 -g -std=c++1y -stdlib=libc++ -I . -I ./linux/tmp/boost_1_57_0 dsl.cpp -I ./mylibs ./linux/tmp/lib/libboost_program_options.a

install_boost_linux:
	test -d linux/tmp/boost_1_57_0 || (mkdir -p linux/tmp && cd linux/tmp && wget -O boost_1_57_0.tar.gz http://sourceforge.net/projects/boost/files/boost/1.57.0/boost_1_57_0.tar.gz/download && tar xzvf boost_1_57_0.tar.gz   &&   temp="$$(pwd)" && cd boost_1_57_0 && ./bootstrap.sh --prefix="$$temp" --with-libraries=program_options && ./b2 toolset=clang cxxflags="-stdlib=libc++" linkflags="-stdlib=libc++" install )

dist_linux:
	vbox import dsl_linux ubuntu1404
	cd linux && vbox dsl_linux up-sync && make prov && vbox dsl_linux halt && sleep 5 &&  cd .. && vbox dsl_linux sharedfolder
	vbox dsl_linux up-sync && ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null prov@33.33.33.254 'cd /media/sf_* && make dsl_linux'
	mv -f a.out dsl_linux
	vbox dsl_linux halt && sleep 5 && vbox dsl_linux destroy


test:
	./test.sh

.PHONY: all help dsl dsl_linux install_boost_linux dist_linux test

