
#include <boost/variant.hpp>
#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <vector>

#include <cassert>

#include "mylibs/util_cpp/util.h"
#include "mylibs/util_cpp/spirit.h"



#define TARGET_EXTENSION ".uc1"
#define TARGET_PREFIX    "§"





/*
 * Soci {
 * 		codice_socio: string, not empty, UNIQUE 
 * 		codice_fiscale: string, not empty if codice_socio = 666
 *
 * 		ass: Ass, not empty
 *
 * 		circolo: Circoli(ass.circolo)
 * }
 * */

namespace parser {
	using namespace and_spirit;


	enum class NodeType { DEFAULT, SET, FIELD, TYPE_BUILTIN, TYPE_USER, REFINE, COND_IF, CONSTRAINT  };
	using ast = and_spirit::ast<NodeType>;


	// ast
	// set -> field -> type, refine->cond_if..., constraint

	template <typename Iterator>
	struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){

			type_builtin = lexeme[(+(char_ - eol - '}' - ','))[emit<NodeType, NodeType::TYPE_BUILTIN>]];

			//type_user = 
			
			cond_if = lit("if ") >> lexeme[(+(char_ - ',' - '}'))[emit<NodeType, NodeType::COND_IF>]];

			refine = lexeme[(+(char_ - eol - '}' - ',' - "if"))[emit<NodeType, NodeType::REFINE>]] >> -cond_if[push<ast>];

			constraint = string("UNIQUE")[emit_str<NodeType, NodeType::CONSTRAINT>];

			type = (type_builtin[push<ast>] | type_user[push<ast>]) >> *(',' >> refine[push<ast>]) >> -(',' >> constraint[push<ast>]);

			field = (+(char_ - ':' - '}'))[emit<NodeType, NodeType::FIELD>] >> ':' >> type[copy_children<ast>];

			set = (+(char_ - '{'))[emit<NodeType, NodeType::SET>] >> '{' >> *field[push<ast>] >> '}';

			root = *set[push<ast>];
		
		}
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> set;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> field;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> type;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> type_builtin;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> type_user;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> refine;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> constraint;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> cond_if;
		//boost::spirit::qi::rule<Iterator, ast()> root;
	};
}
BOOST_FUSION_ADAPT_STRUCT(
		parser::ast,
		(std::string, name)
		(std::vector<parser::ast>, children)
		(parser::NodeType, type)
)






/*
 * §for set
 * create table §set if not exists (
 * 	...
 * 	§for field in set
 * 	§field ... §set.field ... §field.type NOT NULL //§field -> key  §set.field -> value
 * 	§endfor
 * )
 * §endfor
 * */
namespace templat {

#define TEMPLAT_FOR    "for"
#define TEMPLAT_ENDFOR "endfor"
	
	using _map = boost::make_recursive_variant<
		   std::string, 
		   std::map<std::string, boost::recursive_variant_ >
		>::type;
	//using map = std::map<std::string, std::pair<std::string, _map>>; // key(string) => (type(string), value(variant))
	using map = std::map<std::string, _map>; 

	// ast: value_path -> value_path -> ... | for_sym -> for_in?
	
	namespace parser {
		using namespace and_spirit;

		enum class NodeType { DEFAULT, VALUE_PATH, VALUE_COMPONENT, FOR_SYM, FOR_IN};
		using ast = and_spirit::ast<NodeType>;

		
		template <typename Iterator>
		struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
		//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
			grammar():grammar::base_type(root){

				value_component = (+(char_ - '.'))[emit<NodeType, NodeType::VALUE_COMPONENT>];
				value_path = value_component[copy_data<ast>] >>  -(lit(".") >> value_path[push<ast>]);

				for_sym = (+(char_))[emit<NodeType, NodeType::FOR_SYM>];
				for_in = (+(char_))[emit<NodeType, NodeType::FOR_IN>];

				root = ( lit(TARGET_PREFIX TEMPLAT_FOR) >> for_sym[_val = _1] >> -(lit("in") >> for_in[push<ast>]) )
					| ( lit(TARGET_PREFIX) >> value_path[_val = _1] ); 

			}
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root;
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> value_path;
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> value_component;
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> for_sym;
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> for_in;
			//boost::spirit::qi::rule<Iterator, ast()> root;
		};

	}

	using tgrammar = parser::grammar<std::string::iterator>;

	using symbol_entry = std::pair<std::string, map const * const>;//symbol name -> key relative to map
	using symbol_table = std::map<std::string, symbol_entry>;
	

	//rule: x.y (or.y(not implemented)) -> value; no dot -> key
	bool _val_string(const parser::ast& ast, const map& m, const symbol_table& t, std::string & val) {
		auto const * cur = &m;
		bool found = false;
		and_util::walk(ast, static_cast<std::function<void(const parser::ast&)>>([&](const parser::ast& n){

				if (!n.children.empty()){
					if (t.find(n.name) != t.end()){
						auto s_entry = t.at(n.name);
						cur = &boost::get<map>((s_entry.second)->at(s_entry.first));
					} else {
						cur = &boost::get<map>(cur->at(n.name));
					}
				} else {
					if (cur == &m) {
						if (t.find(n.name) != t.end()){
							auto s_entry = t.at(n.name);
							val = s_entry.first;
						} else {
							val = n.name;
						}
					} else {
						if (t.find(n.name) != t.end()){
							auto s_entry = t.at(n.name);
							const std::string * _val = boost::get<std::string>(&((s_entry.second)->at(s_entry.first)));
							if (_val)
								val = *_val;
							else
								val = n.name;
						} else {
							const std::string * _val = boost::get<std::string>(&(cur->at(n.name)));
							if (_val)
								val = *_val;
							else
								val = n.name;
						}
					}
					found = true;
				}
		}));
		if (found)
			return true;
		else
			return false;
	}
	

	// for now, no dot (ast one element)...
	auto _for_map(const parser::ast& ast, const map& m, const symbol_table & t) -> map const * {
		map const * ret = nullptr;
		if (ast.children.empty())//es. §for sym
			return nullptr;
		auto & n = ast.children[0];
		if (t.find(n.name) != t.end()){
			auto s_entry = t.at(n.name);
			ret = &boost::get<map>((s_entry.second)->at(s_entry.first));
		} else {
			ret = &boost::get<map>(m.at(n.name));
		}
		return ret;
	} 


	bool _apply_val(std::string & s, const map& m, const symbol_table& t);
	bool _apply_for(std::string & s, const std::string& header, const map& m, symbol_table& t);
	
	bool apply(std::string & content, const map & m, symbol_table& t) { 
		while (true) {
			std::string line;
			std::istringstream iss(content);
			std::string regex_m1, regex_m2, regex_w;
			while (std::getline(iss, line)) {

				and_util::regex_match_1("(" TARGET_PREFIX TEMPLAT_FOR " \\S+)", line.begin(), line.end(), regex_m1);
				if (!regex_m1.empty()) {
					regex_m1.append("\n");
					std::string inner = and_util::inner_delim_safe(content, regex_m1, "" TARGET_PREFIX TEMPLAT_ENDFOR ""); 
					//if (inner.empty())//todo: errors...
					auto v = inner;
					if (!_apply_for(v, regex_m1, m, t))
						goto err;
					and_util::replace(content, regex_m1, "");
					and_util::replace(content, inner, v);
					and_util::replace(content, "" TARGET_PREFIX TEMPLAT_ENDFOR "", "");
					goto repeat;
				}
				
				and_util::regex_match_1("(" TARGET_PREFIX "\\S+)", line.begin(), line.end(), regex_m1);
				if (!regex_m1.empty()) {
					std::string v = regex_m1;
					if (!_apply_val(v, m, t))
						goto err;
					and_util::replace(content, regex_m1, v);
					goto repeat;	
				}
			}
done:
			break;
repeat:
			continue;
		} 
		return true;
err:
		return false;
	}
	
	bool _apply_val(std::string & s, const map& m, const symbol_table& t) {
		tgrammar g;
		parser::ast root;
		bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;

		r = _val_string(root, m, t, s);
		return r;
	}

	bool _apply_for(std::string & inner, const std::string& header, const map& m, symbol_table& t) {
		tgrammar g;
		parser::ast root;
		auto h = const_cast<std::string&>(header);
		bool r = boost::spirit::qi::phrase_parse(h.begin(), h.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;



		//get map
		map const * cur = _for_map(root, m, t);
		if (cur == nullptr)
			cur = &m;

		
		std::string s;
		const auto _inner = inner;

		for (const auto& kv : m){
			const auto & k = kv.first;

			//add symbol
			symbol_entry se {k,cur};
			t.emplace(root.name, se);


			if (!apply(inner, m, t))
				return false;
			s.append(inner);
			inner = _inner;
			

			//remove symbol
			t.erase(root.name);
		}


		inner = s;

		return true;
	}
	



}
BOOST_FUSION_ADAPT_STRUCT(
		templat::parser::ast,
		(std::string, name)
		(std::vector<templat::parser::ast>, children)
		(templat::parser::NodeType, type)
)



namespace backend {
	
	namespace sql {


		std::map<std::string, std::string> types {{"string", "VARCHAR(200) NOT NULL"}};



		// generate template map
		void template_map(const ::parser::ast& ast, ::templat::map& map) {
			bool error = false; //todo...
			and_util::walk(ast, static_cast<std::function<void(const ::parser::ast&, ::templat::map*&)>>([&](const ::parser::ast& n, ::templat::map*& cur){
				if (n.type == ::parser::NodeType::SET) {	
					(*cur)[n.name] = ::templat::map();
					cur = boost::get<::templat::map>(&((*cur)[n.name]));
				} else if (n.type == ::parser::NodeType::FIELD) {
					::templat::map f;
					auto & n1 = n.children[0];
					assert(n1.type == ::parser::NodeType::TYPE_BUILTIN || n1.type == ::parser::NodeType::TYPE_USER);//todo
					f["type"] = n1.name;
					(*cur)[n.name] = f;
				}
			}), &map);

		}




		//parser for custom sql
		namespace sql_parser {}

		// parse custom sql;
		// generate sql from custom sql ast + dsl ast
		void parse_and_generate_sql(){}
	}
}


void test(){
	typedef parser::grammar<std::string::iterator> grammar;
	grammar g;
	parser::ast root;
	std::string s = "Soci { field: string, not empty if a, UNIQUE  }";
	std::string t;
	bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
	//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
	if (!r){
		std::cout <<"parse fail\n";
		return;
	}

	int level=0;
	and_util::walk(root, static_cast<void (*)(const parser::ast&, int&)>([](const parser::ast& n, int& level){
			std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			level++;
			}), level);

	
	templat::map tmap;
	backend::sql::template_map(root, tmap);


	std::string tem = "§for set\n..§set ..\n§endfor";
	templat::symbol_table st;
	templat::apply(tem, tmap, st);
	std::cout<<tem<<"\n";
}





/*
 * usage: a.out --base=xx.uc --target=aaa
 * */


void spirit_test() {
	using boost::spirit::standard::char_;
	using boost::spirit::qi::lit;
	using namespace boost::spirit::qi::labels;
	std::string s { "a if g" };
	std::string r;
	auto pf = static_cast<void (*)(const std::vector<char>&, const boost::spirit::unused_type&, bool&)>(parser::print);
	bool ok = boost::spirit::qi::parse(s.begin(), s.end(), (
				(*(char_ - "\n" - "if"))[pf] >> lit("if g")
				), r);
	std::cout<<ok;
}

int main(int ac, char* av[]) {

	test();
	//spirit_test();
	return 0;
	
	namespace po = boost::program_options;
	
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "")
		("base", po::value<std::vector<std::string>>(), "")
		("target", po::value<std::vector<std::string>>(), "")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);
	
	if (!vm.count("base") || !vm.count("target")) {
		std::cout<<desc<<"\n";
		return 1;
	}
	
	const std::string base = vm["base"].as< std::vector<std::string> >()[0];
	const std::string target = vm["target"].as< std::vector<std::string> >()[0];


	typedef parser::grammar<std::string::iterator> grammar;
	grammar g;
	parser::ast root;
	std::string s = and_util::get_file_contents(base.c_str());
	std::string t;
	bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
	//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
	if (!r){
		std::cout <<"parse fail (base)\n";
		return 1;
	}

	//int level=0;
	//and_util::walk(root, static_cast<void (*)(const parser::ast&, int&)>([](const parser::ast& n, int& level){
			//std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			//level++;
			//}), level);


	//std::string s1 = and_util::get_file_contents(target.c_str());
	//if (!generator::apply(s1, parser::generate(root))){
		//std::cout<<"parse fail (target)\n";
		//return 1;
	//}


	//std::string f = and_util::replace(target, TARGET_EXTENSION, "");
	//std::ofstream fout(f);
	//fout << s1;


}


