
#include <boost/variant.hpp>

#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <vector>

#include "mylibs/util_cpp/util.h"
#include "mylibs/util_cpp/spirit.h"



#define TARGET_EXTENSION ".uc1"
#define TARGET_PREFIX    "§"





namespace backend {
	//ios, android, ..
	//plain: backend that performs a simple substitution...
}


namespace parser {
	using namespace and_spirit;


	enum class NodeType { DEFAULT, MAP, KEY, VALUE, };
	using ast = and_spirit::ast<NodeType>;

	// ast -> std::map..
	using _map = boost::make_recursive_variant<
		   std::string, 
		   std::map<std::string, boost::recursive_variant_ >
		>::type;
	using map = std::map<std::string, _map>;
	auto generate(const ast& root) -> map {
		map m;
		//auto * cur = &m;
		and_util::walk(root, static_cast<std::function<void(const parser::ast&, map*&)>>([&](const parser::ast& n, map*& cur){
				if (n.type == NodeType::DEFAULT || n.name.empty()){
					
				} else if (n.type == NodeType::MAP) {
					(*cur)[n.name] = map();
					cur = boost::get<map>(&((*cur)[n.name]));
				} else if (n.type == NodeType::KEY) {
					(*cur)[n.name] = n.children[0].name;
				}
		}),&m);
		return m;
	}

	//ast: map -> map | (key->value)

	template <typename Iterator>
	struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){

			value = lexeme[(+(char_ - eol - '}'))[emit<NodeType, NodeType::VALUE>]];

			key = (+(char_ - '=' - '}'))[emit<NodeType, NodeType::KEY>] >> '=' >> value[push<ast>];

			map = (+(char_ - '{'))[emit<NodeType, NodeType::MAP>] >> '{' >> *key[push<ast>] >> '}';

			root = *map[push<ast>];
		
		}
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> map;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> key;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> value;
		//boost::spirit::qi::rule<Iterator, ast()> root;
		//boost::spirit::qi::rule<Iterator, ast()> map;
		//boost::spirit::qi::rule<Iterator, ast()> prop;
	};
}
BOOST_FUSION_ADAPT_STRUCT(
		parser::ast,
		(std::string, name)
		(std::vector<parser::ast>, children)
		(parser::NodeType, type)
)


namespace generator {
	// find text to replace in input file
	// parse input file context
	// parse each text
	// apply backend to parsed text, based on context
	// perform substitution


	namespace parser {
		using namespace and_spirit;

		enum class NodeType { DEFAULT, VALUE_PATH, VALUE_COMPONENT};
		using ast = and_spirit::ast<NodeType>;

		
		template <typename Iterator>
		struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
		//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
			grammar():grammar::base_type(root){

				value_component = (+(char_ - '.'))[emit<NodeType, NodeType::VALUE_COMPONENT>];

				value_path = value_component[copy_data<ast>] >>  -(lit(".") >> value_path[push<ast>]);

				root = lit(TARGET_PREFIX) >> value_path[_val = _1];

			}
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root;
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> value_path;
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> value_component;
			//boost::spirit::qi::rule<Iterator, ast()> root;
			//boost::spirit::qi::rule<Iterator, ast()> map;
			//boost::spirit::qi::rule<Iterator, ast()> prop;
		};

	}
	
	
	
	bool apply(std::string& content, const ::parser::map& m) {
		while (true) {
			std::string s;
			and_util::regex_match_1("(" TARGET_PREFIX "\\S+)", content.begin(), content.end(), s);
			if (s.empty())
				goto ok;
			
			typedef parser::grammar<std::string::iterator> grammar;
			grammar g;
			parser::ast root;
			bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
			if (!r)
				goto error;

			auto * cur = &m;
			std::string val;
			and_util::walk(root, static_cast<std::function<void(const parser::ast&)>>([&](const parser::ast& n){

					if (!n.children.empty()){
						cur = &boost::get<::parser::map>(cur->at(n.name));
					} else
						val = boost::get<std::string>(cur->at(n.name));
			}));
			

			//std::cout <<val;
			//break;

			and_util::replace(content, s, val);

		}

error:
		return false;
ok:
		return true;
	}

}
BOOST_FUSION_ADAPT_STRUCT(
		generator::parser::ast,
		(std::string, name)
		(std::vector<generator::parser::ast>, children)
		(generator::parser::NodeType, type)
)









void spirit_test() {
	using boost::spirit::standard::char_;
	using boost::spirit::qi::lit;
	using namespace boost::spirit::qi::labels;
	std::string s { ":;" };
	std::string r;
	auto pf = static_cast<void (*)(const std::vector<char>&, const boost::spirit::unused_type&, bool&)>(parser::print);
	bool ok = boost::spirit::qi::parse(s.begin(), s.end(), (
				(*(char_ - "\n" - ":;"))[pf] >> lit(":")
				), r);
	std::cout<<ok;
}


/*
 * usage: a.out --base=xx.uc --target=aaa
 * */



int main(int ac, char* av[]){
	//spirit_test_variant();
	//return 0;
	
	namespace po = boost::program_options;
	
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "")
		("base", po::value<std::vector<std::string>>(), "")
		("target", po::value<std::vector<std::string>>(), "")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);
	
	if (!vm.count("base") || !vm.count("target")) {
		std::cout<<desc<<"\n";
		return 1;
	}
	
	const std::string base = vm["base"].as< std::vector<std::string> >()[0];
	const std::string target = vm["target"].as< std::vector<std::string> >()[0];


	typedef parser::grammar<std::string::iterator> grammar;
	grammar g;
	parser::ast root;
	std::string s = and_util::get_file_contents(base.c_str());
	std::string t;
	bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
	//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
	if (!r){
		std::cout <<"parse fail (base)\n";
		return 1;
	}

	//int level=0;
	//and_util::walk(root, static_cast<void (*)(const parser::ast&, int&)>([](const parser::ast& n, int& level){
			//std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			//level++;
			//}), level);


	std::string s1 = and_util::get_file_contents(target.c_str());
	if (!generator::apply(s1, parser::generate(root))){
		std::cout<<"parse fail (target)\n";
		return 1;
	}


	std::string f = and_util::replace(target, TARGET_EXTENSION, "");
	std::ofstream fout(f);
	fout << s1;


}


