
#include <boost/variant.hpp>
#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <vector>

#include <cassert>

#include "mylibs/util_cpp/util.h"
#include "mylibs/util_cpp/spirit.h"



#define TARGET_EXTENSION ".uc1"
#define TARGET_PREFIX    "§"






namespace dsl {

namespace uc {
	using namespace and_spirit;


	enum class NodeType { DEFAULT, MAP, KEY, VALUE, };
	using ast = and_spirit::ast<NodeType>;


	//ast: map -> map | (key->value)

	template <typename Iterator>
	struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){

			value = lexeme[(+(char_ - eol - '}'))[emit<NodeType, NodeType::VALUE>]];

			key = (+(char_ - '=' - '}'))[emit<NodeType, NodeType::KEY>] >> '=' >> value[push<ast>];

			map = (+(char_ - '{'))[emit<NodeType, NodeType::MAP>] >> '{' >> *key[push<ast>] >> '}';

			root = *map[push<ast>];
		
		}
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> map;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> key;
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> value;
		//boost::spirit::qi::rule<Iterator, ast()> root;
		//boost::spirit::qi::rule<Iterator, ast()> map;
		//boost::spirit::qi::rule<Iterator, ast()> prop;
	};
}

/*
 * Soci {
 * 		codice_socio: string, not empty, UNIQUE 
 * 		codice_fiscale: string, not empty if codice_socio = 666
 *
 * 		ass: Ass, not empty
 *
 * 		circolo: Circoli(ass.circolo)
 *
 * 		UNIQUE a, b
 * }
 * */
namespace data {
	using namespace and_spirit;

	const char INDENT[] = "<<INDENT>>\n";
	const char DEDENT[] = "<<DEDENT>>\n";

	enum class NodeType { DEFAULT, ENUM, ENUM_CHOICE, SET, FIELD, FIELD_NAME, CONSTRAINT, TYPE_BUILTIN, TYPE_USER, TYPE_ARRAY, REFINE, COND_IF, CONSTRAINT_INLINE, SET_NAME, FUNC, EXPR_CALL  };
	using ast = and_spirit::ast<NodeType>;


	// ast
	// set -> field -> type_user|type_builtin|type_array, refine->cond_if..., constraint_inline
	// set -> constraint -> field_name
	// type_array -> set_name
	// func ->

	template <typename Iterator>
	struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){

			field_name   = (+(char_ - ':' - '}'))[emit<NodeType, NodeType::FIELD_NAME>]; 	
			type_name    = (+(char_ - ':' - '}' - '('))[emit<NodeType, NodeType::DEFAULT>]; 
			type_builtin = lexeme[(+(char_ - eol - '}' - ','))[emit<NodeType, NodeType::TYPE_BUILTIN>]];
			//type_user = 
			type_array   = type_name[copy_name<ast, NodeType, NodeType::TYPE_ARRAY>] >> '(' >> set_name[push<ast>] >> ')'; 
			cond_if      = lit("if ") >> lexeme[(+(char_ - ',' - '}'))[emit<NodeType, NodeType::COND_IF>]];
			refine       = lexeme[(+(char_ - eol - '}' - ',' - "if"))[emit<NodeType, NodeType::REFINE>]] >> -cond_if[push<ast>];
			constraint_inline = string("UNIQUE")[emit_str<NodeType, NodeType::CONSTRAINT_INLINE>];
			type         = (type_builtin[push<ast>] | type_user[push<ast>] | type_array[push<ast>]) 
							>> *(',' >> refine[push<ast>]) >> -(',' >> constraint_inline[push<ast>]);
			field        = field_name[copy_name<ast, NodeType, NodeType::FIELD>] >> ':' >> type[copy_children<ast>];
			constraint   = string("UNIQUE")[emit_str<NodeType, NodeType::CONSTRAINT>] >> field_name[push<ast>] >> *(',' >> field_name[push<ast>]);
			set_name     = (+(char_ - '{'))[emit<NodeType, NodeType::SET_NAME>];;
			set          = set_name[copy_name<ast, NodeType, NodeType::SET>] >> '{' >> *field[push<ast>] >> *constraint[push<ast>] >> '}';
			enum_choice  = field_name[copy_name<ast, NodeType, NodeType::ENUM_CHOICE>];
			enumm        = lit("enum") >> set_name[copy_name<ast, NodeType, NodeType::ENUM>] >> '{' >> *enum_choice[push<ast>] >> '}';


			//variable  = (+(char_ - ':' - '}'))[emit<NodeType, NodeType::>]; 	
			//func_name = (+(char_ - ':'))[emit<NodeType, NodeType::FUNC>];
			//expr_call = func_name[copy_name<ast, NodeType, NodeType::EXPR_CALL>];
			//expr_if   = lit("if") >> lit(INDENT) >> (expr_if_custom | ) >> -expr_else >> lit(DEDENT);
			//expr_if_custom = lexeme[(+(char_ - '\n'))[emit<>]];
			//expr_if_empty = variable >> lit("is empty"); 
			//expr_get
			//expr_put
			//expr_new
			//expr_ok
			//expr_fail
			////expr_get  = ;
			////expr_put  =;
			//expr      = expr_call[copy_data<ast>];
			//func      = func_name[copy_data<ast>] >> ':' >> lit(INDENT) >> *expr[push<ast>] >> lit(DEDENT);


			root = *set[push<ast>];
		
		}
		boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root, set, field, field_name, type, type_builtin, type_user, refine, constraint_inline, constraint, cond_if, enumm, enum_choice, set_name, type_array, type_name, func, func_name, expr, expr_call;
		//boost::spirit::qi::rule<Iterator, ast()> root;
	};
}
}
BOOST_FUSION_ADAPT_STRUCT(
		dsl::uc::ast,
		(std::string, name)
		(std::vector<dsl::uc::ast>, children)
		(dsl::uc::NodeType, type)
)
BOOST_FUSION_ADAPT_STRUCT(
		dsl::data::ast,
		(std::string, name)
		(std::vector<dsl::data::ast>, children)
		(dsl::data::NodeType, type)
)






/*
 * §for set
 * create table §set if not exists (
 * 	...
 * 	§for field in set
 * 	§field ... §set.field ... §field.type NOT NULL //§field -> key  §set.field -> value
 * 	§endfor
 * )
 * §endfor
 * */
namespace templat {

#define TEMPLAT_FOR    "for"
#define TEMPLAT_ENDFOR "endfor"
	
	using _map = boost::make_recursive_variant<
		   std::string, 
		   std::map<std::string, boost::recursive_variant_ >
		>::type;
	//using map = std::map<std::string, std::pair<std::string, _map>>; // key(string) => (type(string), value(variant))
	using map = std::map<std::string, _map>; 

	// ast: value_com -> value_com -> ... | for_sym -> (value_com->value_com->..)?
	
	namespace parser {
		using namespace and_spirit;

		enum class NodeType { DEFAULT, VALUE_COMPONENT, FOR_SYM, };
		using ast = and_spirit::ast<NodeType>;

		
		template <typename Iterator>
		struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::standard::space_type> {
		//struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
			grammar():grammar::base_type(root){

				value_component = (+(char_("a-zA-Z0-9_") - '.'))[emit<NodeType, NodeType::VALUE_COMPONENT>];
				value_path = value_component[copy_data<ast>] >>  -(lit(".") >> value_path[push<ast>]);

				for_sym = (+(char_))[emit<NodeType, NodeType::FOR_SYM>];
				//for_in = (+(char_))[emit<NodeType, NodeType::FOR_IN>];
				for_in = value_path[_val = _1];

				root = ( lit(TARGET_PREFIX TEMPLAT_FOR) >> for_sym[_val = _1] >> -(lit("in") >> for_in[push<ast>]) )
					| ( lit(TARGET_PREFIX) >> value_path[_val = _1] ); 

			}
			boost::spirit::qi::rule<Iterator, ast(), boost::spirit::standard::space_type> root, value_path, value_component, for_sym, for_in;
			//boost::spirit::qi::rule<Iterator, ast()> root;
		};

	}

	using tgrammar = parser::grammar<std::string::iterator>;

	using symbol_entry = std::pair<std::string, map const * const>;//symbol name -> key relative to map
	using symbol_table = std::map<std::string, symbol_entry>;
	

	//rule: x.y (or.y(not implemented)) -> value; no dot -> key
		template <typename Val, typename K>
		void _key(Val const *& val, const K& k ) {}
		template <>
		void _key<std::string, std::string>(std::string const *& val, const std::string& k ) {
			val = &k;
		}
	template <typename Val>
	auto _val(const parser::ast& ast, const map& m, const symbol_table& t, const bool can_return_key = true) -> Val const* {
		auto const * cur = &m;
		bool found = true;
		Val const * val = nullptr;
		and_util::walk(ast, static_cast<std::function<void(const parser::ast&)>>([&](const parser::ast& n){

				if (!found) return;

				if (!n.children.empty()){
					if (t.find(n.name) != t.end()){
						auto s_entry = t.at(n.name);
						cur = &boost::get<map>((s_entry.second)->at(s_entry.first));
					} else {
						if (cur->find(n.name) != cur->end())
							cur = &boost::get<map>(cur->at(n.name));
						else {
							found = false;
						}
					}
				} else {
					if (cur == &m && can_return_key) {
						if (t.find(n.name) != t.end()){
							auto s_entry = t.at(n.name);
							_key(val, s_entry.first);
							//val = &s_entry.first;
						} else {
							_key(val, n.name);
							//val = &n.name;
						}
					} else {
						if (t.find(n.name) != t.end()){
							auto s_entry = t.at(n.name);
							Val const * _val = boost::get<Val>(&((s_entry.second)->at(s_entry.first)));
							if (_val)
								val = _val;
							else if (can_return_key)
								_key(val, n.name);
								//val = &n.name;
							else
								found = false;
						} else {
							if (cur->find(n.name) != cur->end()){
								Val const * _val = boost::get<Val>(&(cur->at(n.name)));
								if (_val)
									val = _val;
								else if (can_return_key)
									_key(val, n.name);
									//val = &n.name;
								else
									found = false;
							} else {
								found = false;
							}
						}
					}
				}
		}));
		//return found;
		return val;
	}

	


	bool _apply_val(std::string & s, const map& m, const symbol_table& t);
	bool _apply_for(std::string & s, const std::string& header, const map& m, symbol_table& t);
	
	bool apply(std::string & content, const map & m, symbol_table& t) { 
		while (true) {
			std::string line;
			std::istringstream iss(content);
			std::string regex_m1, regex_m2, regex_w;
			while (std::getline(iss, line)) {

				and_util::regex_match_1("(" TARGET_PREFIX TEMPLAT_FOR " \\S+)", line.begin(), line.end(), regex_m1);
				if (!regex_m1.empty()) {
					regex_m1.append("\n");
					std::string inner = and_util::inner_delim_safe(content, regex_m1, "" TARGET_PREFIX TEMPLAT_ENDFOR ""); 
					//if (inner.empty())//todo: errors...
					auto v = inner;
					if (!_apply_for(v, regex_m1, m, t))
						goto err;
					boost::algorithm::replace_first(content, regex_m1, "");
					boost::algorithm::replace_first(content, inner, v);
					boost::algorithm::replace_first(content, "" TARGET_PREFIX TEMPLAT_ENDFOR "", "");
					goto repeat;
				}
				
				//and_util::regex_match_1("(" TARGET_PREFIX "\\S+)", line.begin(), line.end(), regex_m1);
				and_util::regex_match_1("(" TARGET_PREFIX "[a-zA-Z0-9_.]+)", line.begin(), line.end(), regex_m1);
				if (!regex_m1.empty()) {
					std::string v = regex_m1;
					if (!_apply_val(v, m, t)){
						//goto err;
						boost::algorithm::replace_first(content, TARGET_PREFIX, "__(/[[no found]]))__");
					} else {
						boost::algorithm::replace_first(content, regex_m1, v);
					}
					goto repeat;	
				}
			}
done:
			break;
repeat:
			continue;
		} 
		boost::algorithm::replace_all(content, "__(/[[no found]]))__", TARGET_PREFIX); 
		return true;
err:
		return false;
	}
	
	bool _apply_val(std::string & s, const map& m, const symbol_table& t) {
		tgrammar g;
		parser::ast root;
		bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;

		std::string const * _s = _val<std::string>(root, m, t);
		s = *_s;
		return r;
	}

	bool _apply_for(std::string & inner, const std::string& header, const map& m, symbol_table& t) {
		tgrammar g;
		parser::ast root;
		auto h = const_cast<std::string&>(header);
		bool r = boost::spirit::qi::phrase_parse(h.begin(), h.end(), g, boost::spirit::standard::space, root);
		if (!r)
			return false;



		//get map
		map const * cur = nullptr;
		if (!root.children.empty()) {//es. §for sym
			auto & n = root.children[0];
			cur = _val<map>(n, m, t, false);
		}
		if (cur == nullptr)
			cur = &m;

		
		std::string s;
		const auto _inner = inner;

		for (const auto& kv : m){
			const auto & k = kv.first;

			//add symbol
			symbol_entry se {k,cur};
			t.emplace(root.name, se);


			if (!apply(inner, m, t))
				return false;
			s.append(inner);
			inner = _inner;
			

			//remove symbol
			t.erase(root.name);
		}


		inner = s;

		return true;
	}
	



}
BOOST_FUSION_ADAPT_STRUCT(
		templat::parser::ast,
		(std::string, name)
		(std::vector<templat::parser::ast>, children)
		(templat::parser::NodeType, type)
)


//dsl ast -> templat, ...
namespace backend {

	namespace uc {
		void template_map(const ::dsl::uc::ast& root, ::templat::map& m)  {
			and_util::walk(root, static_cast<std::function<void(const ::dsl::uc::ast&, ::templat::map*&)>>([&](const ::dsl::uc::ast& n, ::templat::map*& cur){
					if (n.type == ::dsl::uc::NodeType::DEFAULT || n.name.empty()){
						
					} else if (n.type == ::dsl::uc::NodeType::MAP) {
						(*cur)[n.name] = ::templat::map();
						cur = boost::get<::templat::map>(&((*cur)[n.name]));
					} else if (n.type == ::dsl::uc::NodeType::KEY) {
						(*cur)[n.name] = n.children[0].name;
					}
			}),&m);
		}
	}
	

	namespace sql {

		std::map<std::string, std::string> types {{"string", "VARCHAR(200) NOT NULL"}};

		// {"set1": { "constraints": "a__b": { "a":"a", "b":"b" },    "fields": { "field1": { "type": "a" } } }
		void template_map(const ::dsl::data::ast& ast, ::templat::map& map) {
			bool error = false; //todo...
			and_util::walk(ast, static_cast<std::function<void(const ::dsl::data::ast&, ::templat::map*&)>>([](const ::dsl::data::ast& n, ::templat::map*& cur){
				if (n.type == ::dsl::data::NodeType::SET) {	
					::templat::map m;
					m["fields"] = ::templat::map();
					m["constraints"] = ::templat::map();
					(*cur)[n.name] = m;
					cur = boost::get<::templat::map>(&((*cur)[n.name]));
				} else if (n.type == ::dsl::data::NodeType::CONSTRAINT) {
					std::string name;
					::templat::map cc;
					for (auto const & c : n.children) {
						name.append(c.name + "__");
						cc[c.name] = c.name;
					}
					boost::algorithm::erase_last(name, "__");
					auto & ccc = boost::get<::templat::map>((*cur)["constraints"]);
					ccc[name] = cc;
					
				} else if (n.type == ::dsl::data::NodeType::FIELD_NAME) {

				} else if (n.type == ::dsl::data::NodeType::FIELD) {
					::templat::map f;
					auto & n1 = n.children[0];
					assert(n1.type == ::dsl::data::NodeType::TYPE_BUILTIN || n1.type == ::dsl::data::NodeType::TYPE_USER);//todo
					f["type"] = n1.name;
					auto & ff = boost::get<::templat::map>((*cur)["fields"]);
					ff[n.name] = f;
				}
			}), &map);

		}




		//parser for custom sql
		namespace sql_parser {}

		// parse custom sql;
		// generate sql from custom sql ast + dsl ast
		void parse_and_generate_sql(){}
	}
}


void test(){
	typedef dsl::data::grammar<std::string::iterator> grammar;
	grammar g;
	dsl::data::ast root;
	std::string s = "Soci { field: string, not empty if a, UNIQUE  }";
	std::string t;
	bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
	//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
	if (!r){
		std::cout <<"parse fail\n";
		return;
	}

	int level=0;
	and_util::walk(root, static_cast<void (*)(const dsl::data::ast&, int&)>([](const dsl::data::ast& n, int& level){
			std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			level++;
			}), level);

	
	templat::map tmap;
	backend::sql::template_map(root, tmap);


	std::string tem = "§for set\n..§set ..\n§endfor";
	templat::symbol_table st;
	templat::apply(tem, tmap, st);
	std::cout<<tem<<"\n";
}





/*
 * usage: a.out --base=xx.uc --target=aaa --dsl=bbb
 * */


//void spirit_test() {
	//using boost::spirit::standard::char_;
	//using boost::spirit::qi::lit;
	//using namespace boost::spirit::qi::labels;
	//std::string s { "a if g" };
	//std::string r;
	//auto pf = static_cast<void (*)(const std::vector<char>&, const boost::spirit::unused_type&, bool&)>(parser::print);
	//bool ok = boost::spirit::qi::parse(s.begin(), s.end(), (
				//(*(char_ - "\n" - "if"))[pf] >> lit("if g")
				//), r);
	//std::cout<<ok;
//}

int main(int ac, char* av[]) {

	//test();
	//spirit_test();
	//return 0;
	
	namespace po = boost::program_options;
	
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "")
		("base", po::value<std::vector<std::string>>(), "")
		("target", po::value<std::vector<std::string>>(), "")
		("dsl", po::value<std::vector<std::string>>(), "")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);
	
	if (!vm.count("base") || !vm.count("target") || !vm.count("dsl") ) {
		std::cout<<desc<<"\n";
		return 1;
	}
	
	const std::string base   = vm["base"].as< std::vector<std::string> >()[0];
	const std::string target = vm["target"].as< std::vector<std::string> >()[0];
	const std::string dsl    = vm["dsl"].as< std::vector<std::string> >()[0];


	if (dsl == "uc") {
		typedef dsl::uc::grammar<std::string::iterator> grammar;
		grammar g;
		dsl::uc::ast root;
		std::string s = and_util::get_file_contents(base.c_str());
		std::string t;
		bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::standard::space, root);
		//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
		if (!r){
			std::cout <<"parse fail (base)\n";
			return 1;
		}

		std::string s1 = and_util::get_file_contents(target.c_str());
	
		templat::map tmap;
		backend::uc::template_map(root, tmap);


		templat::symbol_table st;
		templat::apply(s1, tmap, st);


		std::string f = boost::algorithm::replace_first_copy(target, TARGET_EXTENSION, "");
		std::ofstream fout(f);
		fout << s1;
	}

	if (dsl == "data") {
		typedef dsl::data::grammar<std::string::iterator> grammar;
		grammar g;
		dsl::data::ast root;
		std::string s = and_util::get_file_contents(base.c_str());
		std::string indented;
		std::istringstream iss(s);
		and_spirit::indent(iss, indented, dsl::data::INDENT, dsl::data::DEDENT, std::pair<const char*, const char*>("{", "}"));
		std::string t;
		bool r = boost::spirit::qi::phrase_parse(indented.begin(), indented.end(), g, boost::spirit::standard::space, root);
		//bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
		if (!r){
			std::cout <<"parse fail (base)\n";
			return 1;
		}

		std::string s1 = and_util::get_file_contents(target.c_str());
	
		templat::map tmap;
		backend::sql::template_map(root, tmap);


		//std::string s1 = "§for set\n..§set ..\n§endfor";
		templat::symbol_table st;
		templat::apply(s1, tmap, st);
		//std::cout<<tem<<"\n";


		std::string f = boost::algorithm::replace_first_copy(target, TARGET_EXTENSION, "");
		std::ofstream fout(f);
		fout << s1;
	}

	//int level=0;
	//and_util::walk(root, static_cast<void (*)(const parser::ast&, int&)>([](const parser::ast& n, int& level){
			//std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			//level++;
			//}), level);




}


