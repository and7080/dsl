#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <vector>

#include "mylibs/util_cpp/util.h"




namespace generator {
}



namespace parser {
	using boost::spirit::qi::lexeme;
	using boost::spirit::ascii::char_;
	using boost::spirit::qi::int_;
	using namespace boost::spirit::qi::labels;
	using boost::phoenix::at_c;
	using boost::phoenix::push_back;
	using boost::spirit::qi::lit;
	using boost::spirit::qi::eol;

	enum class NodeTextType { DEFAULT, TITLE, TITLE1, TITLE_INDENT, TITLE1_INDENT, CONTENT, };

	struct ast {
		std::string name;
		std::vector<ast> children;
		NodeTextType type;

		auto begin() const -> decltype(children.begin()) { return children.begin(); }
		auto end() const -> decltype(children.end()) { return children.end(); }
	};


	// http://stackoverflow.com/questions/3066701/boost-spirit-semantic-action-parameters
	
	typedef boost::spirit::context<
		boost::fusion::cons<ast&, boost::fusion::nil>,
		boost::fusion::vector0<> > context;

	
	template <NodeTextType type, bool add_nl>
	void emit(const std::vector<char>& a, const context& ctx, bool& mFlag) {
		std::string s;
		s.append(&a[0], a.size());
		if (add_nl) s.append("\n");
		(boost::fusion::at_c<0>(ctx.attributes)).name += s;
		(boost::fusion::at_c<0>(ctx.attributes)).type = type;
	}

	void copy_data(const ast& a, const context& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).name = a.name;
		(boost::fusion::at_c<0>(ctx.attributes)).type = a.type;
	}
	
	void copy_children(const ast& a, const context& ctx, bool& mFlag) {
		(boost::fusion::at_c<0>(ctx.attributes)).children = a.children;
	}
	
	void push(const ast& a, const context& ctx, bool& mFlag) {
		if (a.type != NodeTextType::DEFAULT)
			(boost::fusion::at_c<0>(ctx.attributes)).children.push_back(a);
	}
	
	void print(const boost::spirit::unused_type& a, const boost::spirit::unused_type& ctx, bool& mFlag) {
		std::cout<<"*";
	}
	void print(const std::vector<char>& a, const boost::spirit::unused_type& ctx, bool& mFlag) {
		std::string s;
		s.append(&a[0], a.size());
		std::cout<<s;
	}






/*

grammar 		= body | body1

block           = title [body] CLOSE | title INDENT body DEDENT CLOSE
body            = content* (block* | block1*) content*

block1          = title1 [body1]
body1           = INDENT content* block1* content* DEDENT

title   		= OPEN (NO-NEWLINE)* NEWLINE
title1   		= (NO-NEWLINE)* ":" NEWLINE
content 		= !title !title1 !INDENT !DEDENT (NO-NEWLINE)* NEWLINE


*/

	template <typename Iterator>
	//struct grammar : boost::spirit::qi::grammar<Iterator, ast(), boost::spirit::ascii::space_type> {
	struct grammar : boost::spirit::qi::grammar<Iterator, ast()> {
		grammar():grammar::base_type(root){


			title = lit("OPEN") >> (+(char_ - eol)) [emit<NodeTextType::TITLE, false>]  >> eol;

			content = *(!title >> !title1 >> !lit("CLOSE") >> +(char_ - eol) >> eol) [emit<NodeTextType::CONTENT, true>] ;

			block = ( title [copy_data] >> -body [copy_children] >> lit("CLOSE") ) 
				| ( title [copy_data] >> lit("INDENT") >> body [copy_children] >> lit("DEDENT") >> lit("CLOSE") );

			body = -content [push] >> -(+block[push] | +block1[push]) >> -content[push];

			title1 = (+(char_ - "\n" - ":\n"))[emit<NodeTextType::TITLE1, false>] >> lit(":") >> eol; 

			block1 = title1[copy_data] >> -body1[copy_children];

			body1 = lit("INDENT") >> -content[push] >> *block1[push] >> -content[push] >> lit("DEDENT");

			root = (body[_val = _1] | body1[_val = _1]);
		
		}
		//boost::spirit::qi::rule<Iterator, ast(), boost::spirit::ascii::space_type> root;
		boost::spirit::qi::rule<Iterator, ast()> root;
		boost::spirit::qi::rule<Iterator, ast()> title;
		boost::spirit::qi::rule<Iterator, ast()> title1;
		boost::spirit::qi::rule<Iterator, ast()> body;
		boost::spirit::qi::rule<Iterator, ast()> body1;
		boost::spirit::qi::rule<Iterator, ast()> block;
		boost::spirit::qi::rule<Iterator, ast()> block1;
		boost::spirit::qi::rule<Iterator, ast()> content;
	};
}
BOOST_FUSION_ADAPT_STRUCT(
		text::ast,
		(std::string, name)
		(std::vector<text::ast>, children)
		(text::NodeTextType, type)
)






void spirit_test() {
	using boost::spirit::ascii::char_;
	using boost::spirit::qi::lit;
	using namespace boost::spirit::qi::labels;
	std::string s { ":;" };
	std::string r;
	auto pf = static_cast<void (*)(const std::vector<char>&, const boost::spirit::unused_type&, bool&)>(text::print);
	bool ok = boost::spirit::qi::parse(s.begin(), s.end(), (
				(*(char_ - "\n" - ":;"))[pf] >> lit(":")
				), r);
	std::cout<<ok;
}

void spirit_test_variant() {
	using boost::spirit::ascii::char_;
	using boost::spirit::qi::lit;
	using namespace boost::spirit::qi::labels;
	std::string s { "b" };
	std::string r;
	bool ok = boost::spirit::qi::parse(s.begin(), s.end(), (
				(char_('a')[std::cout<<_1] | char_('b')[std::cout<<_1])
				), r);
}



int main(){
	//spirit_test_variant();
	//return 0;
	
	typedef text::grammar<std::string::iterator> grammar;
	grammar g;
	text::ast root;
	//std::string s { "ooo\nOPENfhjk8\naaa\nbbb\nCLOSE" };
	//std::string s { "ooo\nOPENfhjk8\naaa\nbbb\nCLOSEOPENtit2\nccc\nCLOSE" };
	//std::string s { "ooo\nOPENfhjk8\nOPENtit2\naaa\nbbb\nCLOSECLOSE" };
	//std::string s { "ooo\nOPENfhjk8\nOPENtit2\nINDENTaaa\nbbb\nDEDENTCLOSECLOSE" };
	//std::string s { "OPENfhjk8\nCLOSE" };
	std::string s { "fhjk8:\nINDENTbbb\nDEDENT" };
	std::string t;
	//bool r = boost::spirit::qi::phrase_parse(s.begin(), s.end(), g, boost::spirit::ascii::space, root);
	bool r = boost::spirit::qi::parse(s.begin(), s.end(), g, root);
	if (r){
		std::cout <<"ok\n";
	} else {
		std::cout <<"fail\n";
	}

	int level=0;
	//auto aaa = [](const text::ast& n, int& lev){};
	//walk(root, aaa, level);
	walk(root, static_cast<void (*)(const text::ast&, int&)>([](const text::ast& n, int& level){
			std::cout<< level << ": " << static_cast<int>(n.type) << ": " <<n.name<<"\n";
			level++;
			}), level);

	//std::cout<<root.children[0].name<<"\n";
	//std::cout<<root.children[1].name<<"\n";
	//std::cout<<root.children.size()<<"\n";
}

