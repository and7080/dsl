#!/usr/bin/env /usr/local/bin/bash

cat > tmp_test_base <<EOF
fhjk8 { 
	hh = jjj
}

sss {
	qqq = 88
}

nested {
	a {
		b = 4
	}
	b {
		b = 5
	}
}

nested1 {
	a {
		b = 9
	}
	a {
		b = 10
	}
	a {
		b = 11
	}
}

aa = ciao

empty { }

deployment {
	repo {
		name = repo
		temp_dir = /home/ssh_user/tmp
		app_dir = /usr/share/nginx/tess
		name_text = 999
	}
	
}


abc {
	name = ab
}

build {
	llvm {}
}





nginx {
	root = 000
	prod {
		root = root1
		ssl = true
		proxy = node
	}
	api {
		root = root2
		ssl = false
		proxy = node
	}
}

url {
	prod {
		linode = url1
		bo = url2
	}
	api {
		linode = url3
	}
}


graffa {
	hy = { io }
}

EOF

# $1: --noshell
function check() {
	echo "$IN" > tmp_test_target

	A=$(./a.out --base=tmp_test_base --target=tmp_test_target --dsl=uc --test $1)

	if [[ "$EXPECT" != "$A" ]]; then
		echo "failed test $I"
		echo
		echo "expected:"
		echo "$EXPECT""EOF"
		echo
		echo "output:"
		echo "$A""EOF"
		return 1
	fi
	return 0
}






# 1

IN="
bo = §fhjk8.hh
h = §sss.qqq
§aa
§empty
"

EXPECT="
bo = jjj
h = 88
ciao
empty"

I=1

check || exit 1









# 2

IN="
§nested.a.b
§nested.b.b
"

EXPECT="
4
5"

I=2

check || exit 1









# 3

IN="
§for bo in nested
§bo.b
§bo
§endfor
"

EXPECT="
4
a
5
b"

I=3

check || exit 1









# 4

IN="
§for bo in nested1.a
§bo.b
§bo
§endfor
"

EXPECT="
9
0
10
1
11
2"

I=4

check || exit 1







# 5

IN="
§for a in sss
§a
§sss.a
§endfor
"

EXPECT="
qqq
88"

I=5

check || exit 1







# 6

IN="
§if aa = ciao
123345
§endif
"

EXPECT="
123345"

I=6

check || exit 1







# 7

IN="
§if aa
123345
§endif
"

EXPECT="
123345"

I=7

check || exit 1






# 8

IN="
§if build.llvm
jjjj
§endif
"

EXPECT="
jjjj"

I=8

check || exit 1








# 9

IN="
§if notdef
123345
§endif
"

EXPECT=""

I=9

check || exit 1








# 10

IN="
§if deployment.repo
67890
§endif
"

EXPECT="
67890"

I=10

check || exit 1







# 11

IN="
§if deployment
class deployment {
§for repo in deployment.repo
my_deployment { '§repo.name':
'§repo.name',
'§repo.temp_dir',
'§repo.app_dir',
}
§endfor
}
§endif
"

EXPECT="
class deployment {
my_deployment { 'repo':
'repo',
'/home/ssh_user/tmp',
'/usr/share/nginx/tess',
}

}"

I=11

check || exit 1







# 12

IN="
§for a in abcd
§a.name
§endfor

§if not hjjuuj
eer
§endif

§if not abc.name1
looooii
§endif
"

EXPECT="


eer


looooii"

I=12

check || exit 1






# 13

IN="
shell: §(printf \"ciao\");
..§eof(tmp=\$(echo \"aaa bbb\") && echo \${tmp// /,})eof..
"

EXPECT="
shell: ciao;
..aaa,bbb
.."

I=13

check || exit 1




# 13a

IN="
shell: §(printf \"ciao\");
..§eof(tmp=\$(echo \"aaa bbb\") && echo \${tmp// /,})eof..
"

EXPECT="
shell: ;
...."

I=13a

check "--noshell" || exit 1




# 14

IN="
§for t in url
§for s in t
§t §s §t.s
§endfor
§endfor


§if nginx.prod.a = b
§if nginx.api.a = c
ioio
§endif
§endif

§for u in url
{
url => '§url.u.linode',
§u
§for v in nginx.u
--§v
---§nginx.u.v

§endfor
},
§endfor

]

"

EXPECT="
api linode url3

prod bo url2
prod linode url1






{
url => 'url3',
api
--proxy
---node

--root
---root2

--ssl
---false


},
{
url => 'url1',
prod
--proxy
---node

--root
---root1

--ssl
---true


},


]"

I=14

check || exit 1





# 15

IN="
§graffa.hy
"

EXPECT="
{ io }"

I=15

check || exit 1






echo "ok"



